package com.arasu.billing.core.report.repositoryimp;

import java.sql.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.arasu.billing.core.billing.repository.DCLineRepository;
import com.arasu.billing.core.report.repository.DCReportRepository;

@Repository
public class DCReportRepositoryImp implements DCReportRepository {

	@Autowired
	EntityManager entityManager;

	@Override
	public List<Map<String, Object>> getDCDetails(Date fromDate, Date toDate, int customerMasterId, int cityMasterId, int driverId) {

		StringBuilder sqlQuery = new StringBuilder(DCLineRepository.QUERY);
		sqlQuery.append(" where dc.dc_date_and_time between '" + fromDate + "' and '" + toDate + "' ");
		sqlQuery.append(" and dcLine.row_status <> '5' ");

		if (customerMasterId > 0) {
			sqlQuery.append(" and( fromCustomer.id = " + customerMasterId + " or toCustomer.id = " + customerMasterId + ") ");
		}
		if (cityMasterId > 0) {
			sqlQuery.append(" and( fromCity.id = " + cityMasterId + " or toCity.id = " + cityMasterId + ") ");
		}
		if (driverId > 0) {
			sqlQuery.append(" and (dc.driver_id = " + driverId + " or dc.delivered_by=" + driverId + " or dc.amount_received_by="+driverId+") ");
		}
		sqlQuery.append(" GROUP BY dc.id ORDER BY dc.id ");

		Query query = entityManager.createNativeQuery(sqlQuery.toString());
		query.unwrap(SQLQuery.class).setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
		return query.getResultList();

	}

}
