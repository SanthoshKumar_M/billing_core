package com.arasu.billing.core.master.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.arasu.billing.core.common.response.APIResponse;
import com.arasu.billing.core.master.entity.EmployeeMaster;
import com.arasu.billing.core.master.entity.ItemMaster;
import com.arasu.billing.core.master.entity.ItemTypeMaster;
import com.arasu.billing.core.master.repository.ItemMasterRepository;

@Service
public class ItemMasterService {
	@Autowired
	private ItemMasterRepository itemMasterRepository;

	public APIResponse getAllItemDetails() {
		APIResponse apiResponse = new APIResponse();
		apiResponse.setData(convertSuggestionItemList(itemMasterRepository.getAllItems()));
		apiResponse.setError(null != apiResponse.getData() ? null : "Item Name Suggestion Null");
		return apiResponse;
	}

	@Transactional(rollbackFor = RuntimeException.class)
	public APIResponse saveItemDetails(ItemMaster itemMaster) {
		APIResponse apiResponse = new APIResponse();
		itemMaster.setCreateDateTime(new Date());
		apiResponse.setData(itemMasterRepository.save(itemMaster));
		apiResponse.setError(null != apiResponse.getData() ? null : "Item insert Null");
		return apiResponse;
	}

	public APIResponse itemNameSuggestion(String itemName) {
		APIResponse apiResponse = new APIResponse();
		List<Map<String, Object>> searchResult = itemMasterRepository.itemNameSuggestion(itemName);
		apiResponse.setData(convertSuggestionItemList(searchResult));
		apiResponse.setError(null != apiResponse.getData() ? null : "Item Name Suggestion Null");
		return apiResponse;
	}

	public APIResponse getItemDetailsById(int id) {
		APIResponse apiResponse = new APIResponse();
		List<Map<String, Object>> searchResult = itemMasterRepository.getItemDetailsById(id);
		apiResponse.setData(convertSuggestionItemList(searchResult).get(0));
		apiResponse.setError(null != apiResponse.getData() ? null : "Item Null");
		return apiResponse;
	}

	private List<ItemMaster> convertSuggestionItemList(final List<Map<String, Object>> searchResult) {
		List<ItemMaster> itemMasters = new ArrayList<ItemMaster>();
		if (null != searchResult) {
			for (int i = 0; i < searchResult.size(); i++) {
				ItemMaster itemMaster = new ItemMaster();
				Map<String, Object> map = searchResult.get(i);
				itemMaster.setId(Integer.parseInt(map.get("id").toString()));
				itemMaster.setItemName(map.get("itemName").toString());
				itemMaster
						.setItemPrintName(null != map.get("itemPrintName") ? map.get("itemPrintName").toString() : "");
				itemMaster.setRowStatus(Integer.parseInt(map.get("rowStatus").toString()));
				if (null != map.get("itemTypeMasterId")) {
					ItemTypeMaster itemTypeMaster = new ItemTypeMaster();
					itemTypeMaster.setId(Integer.parseInt(map.get("itemTypeMasterId").toString()));
					itemMaster.setItemTypeMaster(itemTypeMaster);
				}
				if (null != map.get("createUserId")) {
					EmployeeMaster employeeMaster = new EmployeeMaster();
					employeeMaster.setId(Integer.parseInt(map.get("createUserId").toString()));
					itemMaster.setCreateUser(employeeMaster);
					itemMaster.setCreateDateTime((Date) map.get("createDateTime"));

				}
				itemMasters.add(itemMaster);
			}
		}

		return itemMasters;

	}

	@Transactional(rollbackFor = RuntimeException.class)
	public APIResponse updateItemDetails(ItemMaster itemMaster) {
		APIResponse apiResponse = new APIResponse();
		itemMaster.setUpdateDateTime(new Date());
		apiResponse.setData(null != itemMasterRepository.save(itemMaster) ? itemMaster : null);
		apiResponse.setError(null != apiResponse.getData() ? null : "Item Master Update in Issue");
		return apiResponse;
	}

	public APIResponse checkItemIsAvailable(String itemName) {
		APIResponse apiResponse = new APIResponse();
		apiResponse.setData(itemMasterRepository.existsByItemName(itemName));
		return apiResponse;
	}

}
