package com.arasu.billing.core.master.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.arasu.billing.core.common.response.APIResponse;
import com.arasu.billing.core.master.entity.BranchMaster;
import com.arasu.billing.core.master.repository.BranchMasterRepository;

@Service
public class BranchMasterService {
	@Autowired
	private BranchMasterRepository branchMasterRepository;

	public APIResponse getAllBranchDetails() {
		APIResponse apiResponse = new APIResponse();
		apiResponse.setData(convertBranchListMaptoList(branchMasterRepository.getAllBranchs()));
		return apiResponse;
	}

	private List<BranchMaster> convertBranchListMaptoList(final List<Map<String, Object>> searchResult) {
		List<BranchMaster> branchMasters = new ArrayList<>();
		if (null != searchResult) {
			for (int i = 0; i < searchResult.size(); i++) {
				BranchMaster branchMaster = new BranchMaster();
				Map<String, Object> map = searchResult.get(i);
				branchMaster.setId(Integer.parseInt(map.get("id").toString()));
				branchMaster.setBranchName(map.get("branchName").toString());
				branchMaster.setPrinterName(map.get("printerName").toString());
				branchMasters.add(branchMaster);
			}
		}
		return branchMasters;
	}
}
