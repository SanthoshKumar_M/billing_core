package com.arasu.billing.core.admin.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.arasu.billing.core.admin.service.DCNumberService;
import com.arasu.billing.core.common.response.APIResponse;

@RestController
public class DCNumberController {
	@Autowired
	private DCNumberService dcNumberService;

	@GetMapping("/getCurrentDCRunningNumber/{branchMasterId}")
	public APIResponse getCurrentDCRunningNumber(@PathVariable int branchMasterId) {
		return dcNumberService.getCurrentDCRunningNumber(branchMasterId);
	}
}
