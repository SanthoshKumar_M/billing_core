package com.arasu.billing.core.master.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.arasu.billing.core.common.response.APIResponse;
import com.arasu.billing.core.master.service.BranchMasterService;

@RestController
public class BranchMasterController {
	@Autowired
	private BranchMasterService branchMasterService;

	@GetMapping("/getAllBranchDetails")
	public APIResponse findAllBranch() {
		return branchMasterService.getAllBranchDetails();
	}

}
