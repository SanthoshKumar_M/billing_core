package com.arasu.billing.core.master.repository;

import java.util.List;
import java.util.Map;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.arasu.billing.core.master.entity.VehicleInsuranceDetailsMaster;

@Repository
public interface VehicleInsuranceDetailsRepository extends JpaRepository<VehicleInsuranceDetailsMaster, Integer> {

	@Query("SELECT "
			+ "vehicleInsurance.id as vehicleInsuranceId,"
			+ "vehicleInsurance.insuranceNumber as vehicleInsuranceNumber,"
			+ "vehicleInsurance.fromDate as vehicleInsuranceFromDate,"
			+ "vehicleInsurance.toDate as vehicleInsuranceToDate,"
			+ "vehicleInsurance.rowStatus as vehicleInsuranceRowStatus,"
			+ "vehicleInsurance.createUser.id as vehicleInsuranceCreateUserId," 
			+ "vehicleInsurance.createDateTime as vehicleInsuranceCreateDateTime,"
			+ "vehicleInsurance.vehicleMaster.id as id,"
			+ "vehicleInsurance.vehicleMaster.vehicleNumber as vehicleNumber,"
			+ "vehicleInsurance.vehicleMaster.vehicleModel as vehicleModel,"
			+ "vehicleInsurance.vehicleMaster.vehicleMakeName as vehicleMakeName,"
			+ "vehicleInsurance.vehicleMaster.vehicleOwnerName as vehicleOwnerName,"
			+ "vehicleInsurance.vehicleMaster.fcDate as fcDate,"
			+ "vehicleInsurance.vehicleMaster.tax as tax," 
			+ "vehicleInsurance.vehicleMaster.permit as permit,"
			+ "vehicleInsurance.vehicleMaster.rowStatus as rowStatus,"
			+ "vehicleInsurance.vehicleMaster.createUser.id as createUserId,"
			+ "vehicleInsurance.vehicleMaster.createDateTime as createDateTime "
			+ "FROM VehicleInsuranceDetailsMaster vehicleInsurance  WHERE vehicleInsurance.vehicleMaster.id= :vehicleMasterId ")
	List<Map<String, Object>> getvehicleMasterDetailsById(int vehicleMasterId);

}
