package com.arasu.billing.core.master.repository;

import java.util.List;
import java.util.Map;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.arasu.billing.core.master.entity.EmployeeMaster;

public interface EmployeeMasterRepository extends JpaRepository<EmployeeMaster, Integer> {

	@Query("SELECT employee.id as id,employee.employeeName as employeeName,employee.mobileNumber as mobileNumber FROM EmployeeMaster employee WHERE employee.employeeName like :employeeName% and employee.rowStatus=1")
	List<Map<String, Object>> employeeNameSuggestion(String employeeName);

	/*
	 * @Query("SELECT employee.id as id," + "employee.employeeName as employeeName,"
	 * + "employee.employeeAddress as employeeAddress," +
	 * "employee.mobileNumber as mobileNumber," +
	 * "employee.dateOfJoin as dateOfJoin," +
	 * "employee.aadhaarNumber as aadhaarNumber," +
	 * "employee.employeeType.id as employeeTypeId," +
	 * "employee.employeeType.cityName as cityName," +
	 * "employee.alternativeMobileNumber as alternativeMobileNumber," +
	 * "employee.phoneNumber as phoneNumber," + "employee.rowStatus as rowStatus," +
	 * "employee.createUser.id as createUserId," +
	 * "employee.createDateTime as createDateTime" +
	 * " FROM EmployeeMaster employee WHERE employee.id= :id ")
	 * 
	 * List<Map<String, Object>> getEmployeeDetailsById(int id);
	 */

	boolean existsByEmployeeName(String employeeName);

	@Query("SELECT employee.id as id,employee.employeeName as employeeName,employee.mobileNumber as mobileNumber,employee.employeeType as employeeType FROM EmployeeMaster employee WHERE employee.rowStatus=1")
	List<Map<String, Object>> getAllEmployeeDetails();
	

}
