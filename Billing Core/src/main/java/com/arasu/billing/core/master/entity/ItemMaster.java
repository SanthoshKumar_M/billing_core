package com.arasu.billing.core.master.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.arasu.billing.core.common.to.BaseEntity;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@Entity
@Table(name = "item_master")
public class ItemMaster extends BaseEntity implements Serializable{
	private static final long serialVersionUID = 1L;

	@Column(name = "item_name", unique = true)
	private String itemName;

	@Column(name = "item_print_name")
	private String itemPrintName;

	@JsonInclude(value = Include.NON_NULL)
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "item_type_master_id")
	private ItemTypeMaster itemTypeMaster;

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getItemPrintName() {
		return itemPrintName;
	}

	public void setItemPrintName(String itemPrintName) {
		this.itemPrintName = itemPrintName;
	}

	public ItemTypeMaster getItemTypeMaster() {
		return itemTypeMaster;
	}

	public void setItemTypeMaster(ItemTypeMaster itemTypeMaster) {
		this.itemTypeMaster = itemTypeMaster;
	}


}
