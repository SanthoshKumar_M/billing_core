package com.arasu.billing.core.common.data;

import java.io.Serializable;
import java.util.List;

import com.arasu.billing.core.master.entity.VehicleInsuranceDetailsMaster;
import com.arasu.billing.core.master.entity.VehicleMaster;

public class VehicleData implements Serializable {
	private static final long serialVersionUID = 1L;

	VehicleMaster vehicleMaster;
	List<VehicleInsuranceDetailsMaster> insuranceDetails;

	public VehicleData() {
		// TODO Auto-generated constructor stub
	}

	public VehicleMaster getVehicleMaster() {
		return vehicleMaster;
	}

	public void setVehicleMaster(VehicleMaster vehicleMaster) {
		this.vehicleMaster = vehicleMaster;
	}

	public List<VehicleInsuranceDetailsMaster> getInsuranceDetails() {
		return insuranceDetails;
	}

	public void setInsuranceDetails(List<VehicleInsuranceDetailsMaster> insuranceDetails) {
		this.insuranceDetails = insuranceDetails;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
