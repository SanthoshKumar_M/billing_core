package com.arasu.billing.core.master.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.arasu.billing.core.common.response.APIResponse;
import com.arasu.billing.core.master.repository.ItemTypeMasterRepository;

@Service
public class ItemTypeMasterService {
	@Autowired
	private ItemTypeMasterRepository itemTypeMasterRepository;

	public APIResponse getAllItemTypeDetails() {
		APIResponse apiResponse = new APIResponse();
		apiResponse.setData(itemTypeMasterRepository.findAll());
		return apiResponse;
	}

}
