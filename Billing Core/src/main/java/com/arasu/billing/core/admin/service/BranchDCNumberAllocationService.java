package com.arasu.billing.core.admin.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.arasu.billing.core.admin.entity.BranchDCNumberAllocation;
import com.arasu.billing.core.admin.repository.BranchDCNumberAllocationRepository;
import com.arasu.billing.core.master.entity.BranchMaster;

@Service
public class BranchDCNumberAllocationService {
	@Autowired
	private BranchDCNumberAllocationRepository branchDCNumberAllocationRepository;

	public BranchDCNumberAllocation getBranchNumber(BranchMaster branchMaster) {
		return branchDCNumberAllocationRepository.findByBranchMaster(branchMaster);
	}

}
