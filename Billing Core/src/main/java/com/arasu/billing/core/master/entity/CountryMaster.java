package com.arasu.billing.core.master.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.arasu.billing.core.common.to.BaseEntity;

@Entity
@Table(name = "country_master")
public class CountryMaster extends BaseEntity {

	@Column(name = "country_name")
	private String countryName;

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

}
