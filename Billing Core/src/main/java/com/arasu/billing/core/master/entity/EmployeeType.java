package com.arasu.billing.core.master.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "employee_type_master")
public class EmployeeType implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(name = "employee_type")
	private String employeeTypeName;

	@Column(name = "level")
	private String employeeLevel;

	@Column(name = "is_login_available")
	private boolean loginAvailable;

	@Column(name = "is_license_availale")
	private boolean licenseAvailable;

	@Column(name = "row_status")
	private int rowStatus;

	@Column(name = "create_user_id")
	private Integer createUser;

	@Column(name = "create_date_and_time")
	private Date createDateTime;

	@Column(name = "update_user_id")
	private Integer updateUser;

	@Column(name = "update_date_and_time")
	private Date updateDateTime;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEmployeeTypeName() {
		return employeeTypeName;
	}

	public void setEmployeeTypeName(String employeeTypeName) {
		this.employeeTypeName = employeeTypeName;
	}

	public String getEmployeeLevel() {
		return employeeLevel;
	}

	public void setEmployeeLevel(String employeeLevel) {
		this.employeeLevel = employeeLevel;
	}

	public boolean isLoginAvailable() {
		return loginAvailable;
	}

	public void setLoginAvailable(boolean loginAvailable) {
		this.loginAvailable = loginAvailable;
	}

	public boolean isLicenseAvailable() {
		return licenseAvailable;
	}

	public void setLicenseAvailable(boolean licenseAvailable) {
		this.licenseAvailable = licenseAvailable;
	}

	public int getCreateUser() {
		return createUser;
	}

	public void setCreateUser(int createUser) {
		this.createUser = createUser;
	}

	public Integer getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(Integer updateUser) {
		this.updateUser = updateUser;
	}

	public Date getCreateDateTime() {
		return createDateTime;
	}

	public void setCreateDateTime(Date createDateTime) {
		this.createDateTime = createDateTime;
	}

	public Date getUpdateDateTime() {
		return updateDateTime;
	}

	public void setUpdateDateTime(Date updateDateTime) {
		this.updateDateTime = updateDateTime;
	}

	public int getRowStatus() {
		return rowStatus;
	}

	public void setRowStatus(int rowStatus) {
		this.rowStatus = rowStatus;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
