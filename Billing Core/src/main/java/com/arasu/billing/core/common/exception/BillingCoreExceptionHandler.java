package com.arasu.billing.core.common.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.arasu.billing.core.common.response.APIResponse;

@ControllerAdvice
public class BillingCoreExceptionHandler {
	@ExceptionHandler
	public ResponseEntity<APIResponse> handleException(RuntimeException ex) {
		APIResponse apiResponse = new APIResponse();
		apiResponse.setError(ex.getMessage());
		apiResponse.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR.value()).body(apiResponse);
	}
	
	
	@ExceptionHandler
	public ResponseEntity<APIResponse> handleException(BillingException ex) {
		APIResponse apiResponse = new APIResponse();
		apiResponse.setError(ex.getMessage());
		apiResponse.setStatus(HttpStatus.BAD_REQUEST.value());
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR.value()).body(apiResponse);
	}

}
