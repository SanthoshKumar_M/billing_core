package com.arasu.billing.core.admin.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.arasu.billing.core.admin.entity.BranchDCNumberAllocation;
import com.arasu.billing.core.master.entity.BranchMaster;

public interface BranchDCNumberAllocationRepository extends JpaRepository<BranchDCNumberAllocation, Integer> {

	BranchDCNumberAllocation findByBranchMaster(BranchMaster branchMaster);
}
