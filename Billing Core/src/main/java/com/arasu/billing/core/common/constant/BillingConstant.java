package com.arasu.billing.core.common.constant;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

public class BillingConstant {
	public static final String EMPTY_STRING = "";

	public static final char TYPE_I = 'i';
	public static final char TYPE_U = 'u';

	public static final String NEW_CUSTOMER = "New Customer Create Via DC";

	public static final Integer ACTIVE = 1;
	public static final Integer PENDING = 2;
	public static final Integer CLOSED = 3;
	public static final Integer CANCELED = 4;
	public static final Integer DELETED = 5;

	public static final String DC_STATUS_ACTIVE_STR = "ACTIVE";
	public static final String DC_STATUS_PENDING_STR = "PENDING";
	public static final String DC_STATUS_CLOSED_STR = "CLOSED";
	public static final String DC_STATUS_CANCELED_STR = "CANCELED";

	public static final  Integer TOPAY = 1;
	public static final Integer PAID = 2;
	public static final Integer ACC = 3;
	
	public static final String TOPAY_PAYMENT_MODE = "TO PAY";
	public static final String PAID_PAYMENT_MODE = "PAID";
	public static final String ACC_PAYMENT_MODE = "ACC";

	public static final Integer PAYMENT_PENDING = 1;
	public static final Integer PAYMENT_DONE = 2;

	public static final String PAYMENT_STATUS_PENDING_STR = "PENDING";
	public static final String PAYMENT_STATUS_DONE_STR = "DONE";

	public static final Integer DELIVERY_STATUS_PENDING = 1;
	public static final Integer DELIVERY_STATUS_DONE = 2;
	public static final Integer DELIVERY_STATUS_INOFFICE = 3;

	public static final String DELIVERY_STATUS_PENDING_STR = "PENDING";
	public static final String DELIVERY_STATUS_DONE_STR = "DONE";
	public static final String DELIVERY_STATUS_INOFFICE_STR = "IN OFFICE";

	public static final Map<Integer, String> DELIVERY_STATUS_MAP = new HashMap<Integer, String>();
	public static final Map<Integer, String> DC_STATUS_MAP = new HashMap<Integer, String>();
	public static final Map<Integer, String> PAYMENT_STATUS_MAP = new HashMap<Integer, String>();
	
	public static final BigDecimal ZERO_AMOUNT = new BigDecimal(0.00);
	
	public static final Integer ZERO_DECIMAL_PLACE = 0;
	public static final Integer ONE_DECIMAL_PLACE = 1;
	public static final Integer TWO_DECIMAL_PLACE = 2;
	
	public static final Integer ZERO = 0;
	public static final Integer ONE = 1;
	public static final Integer TWO = 2;

	static {
		DC_STATUS_MAP.put(ACTIVE, DC_STATUS_ACTIVE_STR);
		DC_STATUS_MAP.put(PENDING, DC_STATUS_PENDING_STR);
		DC_STATUS_MAP.put(CLOSED, DC_STATUS_CLOSED_STR);
		DC_STATUS_MAP.put(CANCELED, DC_STATUS_CANCELED_STR);
		
		DELIVERY_STATUS_MAP.put(DELIVERY_STATUS_PENDING, DELIVERY_STATUS_PENDING_STR);
		DELIVERY_STATUS_MAP.put(DELIVERY_STATUS_DONE, DELIVERY_STATUS_DONE_STR);
		DELIVERY_STATUS_MAP.put(DELIVERY_STATUS_INOFFICE, DELIVERY_STATUS_INOFFICE_STR);

		PAYMENT_STATUS_MAP.put(PAYMENT_PENDING, PAYMENT_STATUS_PENDING_STR);
		PAYMENT_STATUS_MAP.put(PAYMENT_DONE, PAYMENT_STATUS_DONE_STR);
	}
}
