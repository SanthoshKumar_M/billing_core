package com.arasu.billing.core.common.to;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToOne;

import com.arasu.billing.core.master.entity.EmployeeMaster;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
@MappedSuperclass
public class BaseEntity implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(name = "row_status")
	private int rowStatus;

	@JsonInclude(value = Include.NON_NULL)
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "create_user_id")
	private EmployeeMaster createUser;

	@Column(name = "create_date_and_time")
	private Date createDateTime;

	@JsonInclude(value = Include.NON_NULL)
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "update_user_id")
	private EmployeeMaster updateUser;

	@Column(name = "update_date_and_time")
	private Date updateDateTime;
	
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getRowStatus() {
		return rowStatus;
	}

	public void setRowStatus(int rowStatus) {
		this.rowStatus = rowStatus;
	}

	public EmployeeMaster getCreateUser() {
		return createUser;
	}

	public void setCreateUser(EmployeeMaster createUser) {
		this.createUser = createUser;
	}

	public Date getCreateDateTime() {
		return createDateTime;
	}

	public void setCreateDateTime(Date createDateTime) {
		this.createDateTime = createDateTime;
	}

	public EmployeeMaster getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(EmployeeMaster updateUser) {
		this.updateUser = updateUser;
	}

	public Date getUpdateDateTime() {
		return updateDateTime;
	}

	public void setUpdateDateTime(Date updateDateTime) {
		this.updateDateTime = updateDateTime;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}