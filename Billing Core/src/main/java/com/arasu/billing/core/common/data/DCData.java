package com.arasu.billing.core.common.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.arasu.billing.core.billing.entity.DC;
import com.arasu.billing.core.billing.entity.DCLine;
import com.arasu.billing.core.common.to.ReportTO;

public class DCData implements Serializable {
	private static final long serialVersionUID = 1L;

	DC dc;
	List<DCLine> dcLine;
	List<Integer> removeItemIds = new ArrayList<Integer>();
	List<ReportTO> reportTOs = new ArrayList<ReportTO>();
	Map<String, Object> parameters = new HashMap<>();
	public DCData() {
		// TODO Auto-generated constructor stub
	}

	public DC getDc() {
		return dc;
	}

	public void setDc(DC dc) {
		this.dc = dc;
	}

	public List<DCLine> getDcLine() {
		return dcLine;
	}

	public void setDcLine(List<DCLine> dcLine) {
		this.dcLine = dcLine;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public List<Integer> getRemoveItemIds() {
		return removeItemIds;
	}

	public void setRemoveItemIds(List<Integer> removeItemIds) {
		this.removeItemIds = removeItemIds;
	}

	public List<ReportTO> getReportTOs() {
		return reportTOs;
	}

	public void setReportTOs(List<ReportTO> reportTOs) {
		this.reportTOs = reportTOs;
	}

	public Map<String, Object> getParameters() {
		return parameters;
	}

	public void setParameters(Map<String, Object> parameters) {
		this.parameters = parameters;
	}

}
