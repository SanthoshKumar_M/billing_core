package com.arasu.billing.core.common.utils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;

import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.AmazonS3Exception;
import com.arasu.billing.core.common.DateUtil;

@Component
@EnableScheduling
public class DatabaseUtil {
	@Autowired
	private JavaMailSender javaMailSender;

//	@Scheduled(initialDelay = 1000, fixedRate = 10000000)
	@Scheduled(cron = "0 0 0 * * *", zone = "Asia/Calcutta")
	public void backup() {
		String home = System.getProperty("user.home");
//		String filePath =home+"/Downloads/" ;
		String filePath = "D:/Billing/DB/DailyBackup/";
		String fileName = filePath + "DB-BackUp-" + DateUtil.getDateMonthYearDateFormat().format(new Date()) + ".sql";

		String command = String.format("mysqldump -u%s -p%s --databases %s -r %s", "arasu", "arasu", "arasu", fileName);
		Process process;
//		System.out.println("Working" + LocalDateTime.now());
		try {
			process = Runtime.getRuntime().exec(command);
			int processComplete = process.waitFor();
			if (processComplete == 0) {
//				sendEmailWithAttachment(process.getInputStream(), fileName);
				new FileSystemResource(new File(filePath));
				putS3Object(fileName);

			}
//			Files.deleteIfExists(Paths.get(fileName));
		} catch (IOException | InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//		System.out.println("completed" + LocalDateTime.now());

	}

	private void sendEmailWithAttachment(InputStream inputStream, String filePath) {
		try {
			MimeMessage msg = javaMailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(msg, true);
			helper.setTo("arasuregular@gmail.com");
			helper.setSubject("DB Backup " + DateUtil.getDateMonthYearDateFormat().format(new Date()));
			helper.setText("Today's BackUp");
//			helper.addAttachment("backup.sql", new ByteArrayResource(IOUtils.toByteArray(inputStream)));
			helper.addAttachment(new File(filePath).getName(), new FileSystemResource(new File(filePath)));
			javaMailSender.send(msg);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void putS3Object(String filePath) {
		try {
			
			new File(filePath);
			String bucketName = "billingapplication/DB Daily Backup";
			String fileName = new File(filePath).getName();
			AWSCredentials credentials = new BasicAWSCredentials("AKIA3PJP3A67N2NQUFWG","ELYEKPrRfEj9LAeaIX2JhPepA3utIAI3trL7JSLL");
			AmazonS3 s3client = AmazonS3ClientBuilder.standard()
					.withCredentials(new AWSStaticCredentialsProvider(credentials)).withRegion(Regions.AP_SOUTH_1)
					.build();
			if (!s3client.doesBucketExistV2(bucketName)) {
				s3client.createBucket(bucketName);
			}
//			ObjectMetadata metadata = new ObjectMetadata();
//			metadata.setContentLength(file.length());
			s3client.putObject(bucketName, fileName, new File(filePath));
			sendEmail("Successfully placed " + fileName + " into bucket " + bucketName);
		} catch (AmazonS3Exception e) {
			System.err.println(e.getMessage());
		}
	}
	
	private void sendEmail(String message) {
		try {
			MimeMessage msg = javaMailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(msg, true);
			helper.setTo("arasuregular@gmail.com");
			helper.setSubject("DB Backup " + DateUtil.getDateMonthYearDateFormat().format(new Date()));
			helper.setText("Today's BackUp "+ message);
			javaMailSender.send(msg);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
