package com.arasu.billing.core.master.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.arasu.billing.core.master.entity.EmployeeType;

public interface EmployeeTypeRepository extends JpaRepository<EmployeeType, Integer> {

}
