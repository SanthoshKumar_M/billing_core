package com.arasu.billing.core.master.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.arasu.billing.core.common.data.VehicleData;
import com.arasu.billing.core.common.response.APIResponse;
import com.arasu.billing.core.master.service.VehicleMasterService;

@RestController
public class VehicleMasterController {
	@Autowired
	private VehicleMasterService vehicleMasterService;

	@GetMapping("/checkVehicleIsAvailable/{vehicleNumber}")
	public APIResponse checkVehicleIsAvailable(@PathVariable String vehicleNumber) {
		return vehicleMasterService.checkVehicleIsAvailable(vehicleNumber);
	}

	@GetMapping("/getVehicleMasterDetailsById/{id}")
	public APIResponse getVehicleMasterDetailsById(@PathVariable int id) {
		return vehicleMasterService.getVehicleMasterDetailsById(id);
	}

	@GetMapping("/getAllVehicleDetails")
	public APIResponse findAllVehicleDetails() {
		return vehicleMasterService.getAllVehicleDetails();
	}

	@PostMapping("/saveVehicleMasterDetails")
	public APIResponse saveVehicleMasterDetails(@RequestBody VehicleData vehicleData) {
		return vehicleMasterService.saveVehicleMasterDetails(vehicleData);
	}

	@PutMapping("/updateVehicleMasterDetails")
	public APIResponse updateVehicleMasterDetails(@RequestBody VehicleData vehicleData) {
		return vehicleMasterService.updateVehicleMasterDetails(vehicleData);
	}

	@GetMapping("/getVehicleNumberSuggestion/{itemName}")
	public APIResponse vehicleNumberSuggestion(@PathVariable String vehicleNumber) {
		return vehicleMasterService.vehicleNumberSuggestion(vehicleNumber);
	}

}
