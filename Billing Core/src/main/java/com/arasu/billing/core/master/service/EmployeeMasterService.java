package com.arasu.billing.core.master.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.arasu.billing.core.admin.entity.Login;
import com.arasu.billing.core.admin.repository.LoginRepository;
import com.arasu.billing.core.common.data.EmployeeData;
import com.arasu.billing.core.common.response.APIResponse;
import com.arasu.billing.core.master.entity.EmployeeMaster;
import com.arasu.billing.core.master.entity.EmployeeType;
import com.arasu.billing.core.master.repository.EmployeeMasterRepository;
import com.arasu.billing.core.master.repository.EmployeeTypeRepository;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class EmployeeMasterService {
	ObjectMapper mapper = new ObjectMapper();

	@Autowired
	private EmployeeMasterRepository employeeMasterRepository;

	@Autowired
	private EmployeeTypeRepository employeeTypeRepository;

	@Autowired
	private LoginRepository loginRepository;

	public APIResponse getAllEmployeeDetails() {
		APIResponse apiResponse = new APIResponse();
		apiResponse.setData(convertSuggestionEmployeeList(employeeMasterRepository.getAllEmployeeDetails()));
		apiResponse.setError(null != apiResponse.getData() ? null : "Customer Name Suggestion Null");
		return apiResponse;
	}

	public APIResponse getAllEmployeeType() {
		APIResponse apiResponse = new APIResponse();
		apiResponse.setData(employeeTypeRepository.findAll());
		apiResponse.setError(null != apiResponse.getData() ? null : "Employee Type Null");
		return apiResponse;
	}

	@Transactional(rollbackFor = RuntimeException.class)
	public APIResponse saveEmployeeDetails(Map<String, Object> employeeMap) {
		APIResponse apiResponse = new APIResponse();
		EmployeeMaster employeeMaster = mapper.convertValue(employeeMap.get("employeeData"), EmployeeMaster.class);
		Login login = mapper.convertValue(employeeMap.get("loginData"), Login.class);
		employeeMaster = employeeMasterRepository.save(employeeMaster);
		if (employeeMaster.getId() > 0 && employeeMaster.getEmployeeType().isLoginAvailable()) {
			login.setEmployeeMaster(employeeMaster);
			loginRepository.save(login);
		}
		apiResponse.setData(employeeMaster);
		apiResponse.setError(null != apiResponse.getData() ? null : "Employee Not Insert");
		return apiResponse;
	}

	@Transactional(rollbackFor = RuntimeException.class)
	public APIResponse updateEmployeeDetails(Map<String, Object> employeeMap) {
		APIResponse apiResponse = new APIResponse();
		EmployeeMaster employeeMaster = mapper.convertValue(employeeMap.get("employeeData"), EmployeeMaster.class);
		employeeMaster.setUpdateDateTime(new Date());
		Login login = mapper.convertValue(employeeMap.get("loginData"), Login.class);
		employeeMaster = employeeMasterRepository.save(employeeMaster);
		if (null != login && login.getId() > 0) {
			login.setEmployeeMaster(employeeMaster);
			loginRepository.save(login);
		}
		apiResponse.setData(employeeMaster);
		apiResponse.setError(null != apiResponse.getData() ? null : "Employee Not Update");
		return apiResponse;
	}

	public APIResponse employeeNameSuggestion(String employeeName) {
		APIResponse apiResponse = new APIResponse();
		List<Map<String, Object>> searchResult = employeeMasterRepository.employeeNameSuggestion(employeeName);
		apiResponse.setData(convertSuggestionEmployeeList(searchResult));
		apiResponse.setError(null != apiResponse.getData() ? null : "Customer Name Suggestion Null");
		return apiResponse;
	}

	public APIResponse checkEmployeeIsAvailable(String employeeName) {
		APIResponse apiResponse = new APIResponse();
		apiResponse.setData(employeeMasterRepository.existsByEmployeeName(employeeName));
		return apiResponse;
	}

	public APIResponse getEmployeeDetailsById(int id) {
		APIResponse apiResponse = new APIResponse();
		EmployeeData employeeData = new EmployeeData();
		EmployeeMaster employeeMaster = employeeMasterRepository.findById(id).orElse(null);
		employeeData.setEmployeeMaster(employeeMaster);
		if (null != employeeMaster) {
			if (employeeMaster.getEmployeeType().isLoginAvailable()) {
				employeeData.setLogin(convertSuggestionLogin(loginRepository.findByEmployeeId(employeeMaster.getId())));
			}
		}
		apiResponse.setData(employeeData);
		apiResponse.setError(null != employeeMaster ? null : "Employee Null");
		return apiResponse;
	}

	private List<EmployeeMaster> convertSuggestionEmployeeList(final List<Map<String, Object>> searchResult) {
		List<EmployeeMaster> employeeMasters = new ArrayList<>();
		if (null != searchResult) {
			for (int i = 0; i < searchResult.size(); i++) {
				EmployeeMaster employeeMaster = new EmployeeMaster();
				Map<String, Object> map = searchResult.get(i);
				employeeMaster.setId(Integer.parseInt(map.get("id").toString()));
				employeeMaster.setEmployeeName(map.get("employeeName").toString());
				employeeMaster.setMobileNumber(map.get("mobileNumber").toString());
				if (null != map.get("employeeType")) {
					employeeMaster.setEmployeeType((EmployeeType) map.get("employeeType"));
				}
				employeeMasters.add(employeeMaster);
			}
		}

		return employeeMasters;

	}

	private Login convertSuggestionLogin(final List<Map<String, Object>> searchResult) {
		Login login = new Login();
		if (null != searchResult) {
			for (int i = 0; i < searchResult.size(); i++) {
				Map<String, Object> map = searchResult.get(i);
				login.setId(Integer.parseInt(map.get("id").toString()));
				login.setUserName(map.get("userName").toString());
				login.setPassword(map.get("password").toString());
				login.setActiveUser((boolean) map.get("activeUser"));
			}
		}

		return login;

	}

}
