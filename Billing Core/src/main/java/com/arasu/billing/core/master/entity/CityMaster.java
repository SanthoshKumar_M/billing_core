package com.arasu.billing.core.master.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.arasu.billing.core.common.to.BaseEntity;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@Entity
@Table(name = "city_master")
public class CityMaster extends BaseEntity {

	@Column(name = "city_name")
	private String cityName;

	@JsonInclude(value = Include.NON_NULL)
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "state_master_id")
	private StateMaster stateMaster;

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public StateMaster getStateMaster() {
		return stateMaster;
	}

	public void setStateMaster(StateMaster stateMaster) {
		this.stateMaster = stateMaster;
	}

}
