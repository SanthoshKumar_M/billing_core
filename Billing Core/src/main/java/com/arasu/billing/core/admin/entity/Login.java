package com.arasu.billing.core.admin.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.arasu.billing.core.master.entity.EmployeeMaster;

@Entity
@Table(name = "login")
public class Login implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@ManyToOne
	@JoinColumn(name = "employee_id")
	private EmployeeMaster employeeMaster;

	@Column(name = "user_name")
	private String userName;

	@Column(name = "password")
	private String password;

	@Column(name = "is_active_user")
	private boolean activeUser;

	/*
	 * @ManyToOne
	 * 
	 * @JoinColumn(name = "create_user_id") private EmployeeMaster createUser;
	 * 
	 * @Column(name = "create_date_and_time") private Date createDateTime;
	 * 
	 * @ManyToOne
	 * 
	 * @JoinColumn(name = "update_user_id") private EmployeeMaster updateUser;
	 * 
	 * @Column(name = "update_date_and_time") private Date updateDateTime;
	 */

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public EmployeeMaster getEmployeeMaster() {
		return employeeMaster;
	}

	public void setEmployeeMaster(EmployeeMaster employeeMaster) {
		this.employeeMaster = employeeMaster;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isActiveUser() {
		return activeUser;
	}

	public void setActiveUser(boolean activeUser) {
		this.activeUser = activeUser;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
