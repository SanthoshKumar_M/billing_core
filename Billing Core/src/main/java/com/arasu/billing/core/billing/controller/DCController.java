package com.arasu.billing.core.billing.controller;

import java.sql.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.arasu.billing.core.billing.service.DCService;
import com.arasu.billing.core.common.data.DCData;
import com.arasu.billing.core.common.data.DCListTO;
import com.arasu.billing.core.common.response.APIResponse;

@RestController
public class DCController {
	@Autowired
	private DCService dcService;

	@PostMapping("/saveDCDetails")
	public APIResponse saveDCDetails(@RequestBody DCData dcData) {
		return dcService.saveDCDetails(dcData);
	}

	@PutMapping("/updateDCDetails")
	public APIResponse updateDCDetails(@RequestBody DCData dcData) {
		return dcService.updateDCDetails(dcData);
	}

	@GetMapping("/getDCList/{branchMasterId}")
	public APIResponse getDCList(@PathVariable int branchMasterId) {
		return dcService.getDCList(branchMasterId);
	}

	@GetMapping("/getDCDetailsById/{id}")
	public APIResponse getDCDetailsById(@PathVariable int id) {
		return dcService.getDCDetailsById(id);
	}

	@GetMapping("/getDCList/{branchMasterId}/{dCNo}")
	public APIResponse getDCList(@PathVariable int branchMasterId, @PathVariable int dCNo) {
		return dcService.getDCList(branchMasterId, dCNo);
	}

	@GetMapping("/getDCList/{branchMasterIds}/{selectedDate}/{selectedCitIds}")
	public APIResponse getDCList(@PathVariable List<Integer> branchMasterIds, @PathVariable Date selectedDate, @PathVariable List<Integer> selectedCitIds) {
		return dcService.getDCList(branchMasterIds, selectedDate, selectedCitIds);
	}
	
	@PutMapping("/updateDCAllocations")
	public APIResponse updateDCAllocations(@RequestBody List<DCListTO> dcListTO) {
		return dcService.updateDCAllocations(dcListTO);
	}
	
	@GetMapping("/getDriverDCList/{selectedDriverId}")
	public APIResponse getDriverDCList(@PathVariable int selectedDriverId) {
		return dcService.getDriverDCList(selectedDriverId);
	}
	
	@PutMapping("/updateDCTransfer")
	public APIResponse updateDCTransfer(@RequestBody List<DCListTO> dcListTO) {
		return dcService.updateDCTransfer(dcListTO);
	}
	
	@PutMapping("/updateDCCollection")
	public APIResponse updateDCCollection(@RequestBody List<DCListTO> dcListTO) {
		return dcService.updateDCCollection(dcListTO);
	}
	
	@GetMapping("/getOfficeItemDCList/{branchMasterId}")
	public APIResponse getOfficeItemDCList(@PathVariable int branchMasterId) {
		return dcService.getOfficeItemDCList(branchMasterId);
	}

	@PutMapping("/receivedOfficeItemDetails")
	public APIResponse receivedOfficeItemDetails(@RequestBody List<DCListTO> dcListTO) {
		return dcService.receivedOfficeItemDetails(dcListTO);
	}
	
	@GetMapping("/getReceivedOfficeItemDCList/{branchId}")
	public APIResponse getReceivedOfficeItemDCList(@PathVariable int branchId) {
		return dcService.getReceivedOfficeItemDCList(branchId);
	}
	
	@GetMapping("/getMasterReport/{fromDate}/{toDate}/{customerMasterId}/{cityMasterId}/{driverId}")
	public APIResponse getMasterReport(@PathVariable Date fromDate, @PathVariable Date toDate,@PathVariable int customerMasterId,@PathVariable int cityMasterId,@PathVariable int driverId) {
		return dcService.getMasterReport(fromDate, toDate, customerMasterId, cityMasterId, driverId);
	}
	
	@GetMapping("/getMonthlyReport/{fromDate}/{toDate}/{customerMasterId}")
	public APIResponse getMonthlyReport(@PathVariable Date fromDate, @PathVariable Date toDate,@PathVariable int customerMasterId) {
		return dcService.getMonthlyReport(fromDate, toDate, customerMasterId);
	}
}
