package com.arasu.billing.core.admin.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.arasu.billing.core.admin.service.LoginService;
import com.arasu.billing.core.common.response.APIResponse;

@RestController
public class LoginController {
	@Autowired
	private LoginService loginService;

	@GetMapping("/getLoginUserDetails/{userName}/{password}")
	public APIResponse findProductByName(@PathVariable String userName, @PathVariable String password) {
		return loginService.getLoginDetails(userName, password);
	}
}
