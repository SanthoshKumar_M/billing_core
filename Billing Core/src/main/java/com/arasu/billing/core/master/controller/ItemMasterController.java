package com.arasu.billing.core.master.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.arasu.billing.core.common.response.APIResponse;
import com.arasu.billing.core.master.entity.ItemMaster;
import com.arasu.billing.core.master.service.ItemMasterService;

@RestController
public class ItemMasterController {
	@Autowired
	private ItemMasterService itemMasterService;

	@GetMapping("/checkItemNameIsAvailable/{itemName}")
	public APIResponse checkItemIsAvailable(@PathVariable String itemName) {
		return itemMasterService.checkItemIsAvailable(itemName);
	}

	@GetMapping("/getItemDetailsById/{id}")
	public APIResponse findItemById(@PathVariable int id) {
		return itemMasterService.getItemDetailsById(id);
	}

	@GetMapping("/getAllItemDetails")
	public APIResponse findAllItem() {
		return itemMasterService.getAllItemDetails();
	}

	@PostMapping("/saveItemDetails")
	public APIResponse saveItemDetails(@RequestBody ItemMaster itemMaster) {
		return itemMasterService.saveItemDetails(itemMaster);
	}

	@PutMapping("/updateItemDetails")
	public APIResponse updateItemDetails(@RequestBody ItemMaster itemMaster) {
		return itemMasterService.updateItemDetails(itemMaster);
	}

	@GetMapping("/getItemsBySuggestionName/{itemName}")
	public APIResponse itemNameSuggestion(@PathVariable String itemName) {
		return itemMasterService.itemNameSuggestion(itemName);
	}

}
