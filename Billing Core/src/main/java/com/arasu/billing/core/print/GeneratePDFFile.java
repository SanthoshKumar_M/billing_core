package com.arasu.billing.core.print;

import com.arasu.billing.core.common.DateUtil;
import com.arasu.billing.core.common.data.DCData;

public class GeneratePDFFile {
	DCPrint dcPrint = new DCPrint();

	public void generatePDFFile(DCData dcData) {
		StringBuilder dc = new StringBuilder();
		dc.append("DC NO : ");
		dc.append(dcData.getDc().getdCNo());
		dc.append("                            ");
		dc.append("DATE : ");
		dc.append(DateUtil.getDateMonthYearDateFormat().format(dcData.getDc().getdCDate()));
		dc.append("\n");

		dc.append("From : ");
		dc.append(dcData.getDc().getFromCustomerMaster().getCustomerPrintName());
		for (int j = dcData.getDc().getFromCustomerMaster().getCustomerPrintName().length(); j <= 40; j++) {
			dc.append(" ");
		}
		dc.append(dcData.getDc().getFromCustomerMaster().getCityMaster().getCityName());
		dc.append("\n");

		dc.append("To   : ");
		dc.append(dcData.getDc().getToCustomerMaster().getCustomerPrintName());
		for (int j = dcData.getDc().getToCustomerMaster().getCustomerPrintName().length(); j <= 40; j++) {
			dc.append(" ");
		}
		dc.append(dcData.getDc().getToCustomerMaster().getCityMaster().getCityName());
		dc.append("\n");

		dc.append("               Item                                      Qty   ");
		dc.append("\n");
		for (int i = 0; i < dcData.getDcLine().size(); i++) {
			dc.append(" " + i + 1 + " | ");
			dc.append(dcData.getDcLine().get(i).getItemMaster().getItemPrintName());
			int length = dcData.getDcLine().get(i).getItemMaster().getItemPrintName().length();
			for (int j = length; j <= 50; j++) {
				dc.append(" ");
			}
			dc.append("|  ");
			dc.append(dcData.getDcLine().get(i).getQty());
			dc.append("\n");
		}

//		try {
//			Files.write(Paths.get("d:/output.txt"), dc.toString().getBytes(StandardCharsets.UTF_8));
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		dcPrint.rawprint(dc.toString());
	}
}
