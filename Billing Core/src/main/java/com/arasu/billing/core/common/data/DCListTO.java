package com.arasu.billing.core.common.data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class DCListTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private int id;
	private int dCNo;
	private Date dCDate;
	private String fromCustomerName;
	private String toCustomerName;
	private String fromCityName;
	private String toCityName;
	private String itemNames;
	private String branchName;
	private String allocatedDriverName;
	private BigDecimal totalNoOfQty;
	private BigDecimal amount = new BigDecimal(0);
	private String otherChargesDetails;
	private BigDecimal otherChargesAmount = new BigDecimal(0);
	private BigDecimal netAmount = new BigDecimal(0);
	private BigDecimal receivedAmount = new BigDecimal(0);
	private int deliveryStatus;
	private int modeOfPayment;
	private int vehicleId;
	private int driverId;
	private int deliveredBy;
	private Date deliveredDate;
	private int amountReceivedBy;
	private Date amountReceivedDate;
	private int dCAmountCheckBy;
	private Date dCAmountCheckDate;
	private boolean isDelivered;
	private boolean isPaymentRecevied;
	private boolean officeDelivery;
	private boolean officeDelivered;
	private Date officeDeliveredDate;
	private int officeId;
	private int rowStatus;
	private String deliveryByName;
	private String amountReceivedByName;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getdCNo() {
		return dCNo;
	}

	public void setdCNo(int dCNo) {
		this.dCNo = dCNo;
	}

	public Date getdCDate() {
		return dCDate;
	}

	public void setdCDate(Date dCDate) {
		this.dCDate = dCDate;
	}

	public String getFromCustomerName() {
		return fromCustomerName;
	}

	public void setFromCustomerName(String fromCustomerName) {
		this.fromCustomerName = fromCustomerName;
	}

	public String getToCustomerName() {
		return toCustomerName;
	}

	public void setToCustomerName(String toCustomerName) {
		this.toCustomerName = toCustomerName;
	}

	public String getFromCityName() {
		return fromCityName;
	}

	public void setFromCityName(String fromCityName) {
		this.fromCityName = fromCityName;
	}

	public String getToCityName() {
		return toCityName;
	}

	public void setToCityName(String toCityName) {
		this.toCityName = toCityName;
	}

	public String getItemNames() {
		return itemNames;
	}

	public void setItemNames(String itemNames) {
		this.itemNames = itemNames;
	}

	public BigDecimal getTotalNoOfQty() {
		return totalNoOfQty;
	}

	public void setTotalNoOfQty(BigDecimal totalNoOfQty) {
		this.totalNoOfQty = totalNoOfQty;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getOtherChargesDetails() {
		return otherChargesDetails;
	}

	public void setOtherChargesDetails(String otherChargesDetails) {
		this.otherChargesDetails = otherChargesDetails;
	}

	public BigDecimal getOtherChargesAmount() {
		return otherChargesAmount;
	}

	public void setOtherChargesAmount(BigDecimal otherChargesAmount) {
		this.otherChargesAmount = otherChargesAmount;
	}

	public BigDecimal getNetAmount() {
		return netAmount;
	}

	public void setNetAmount(BigDecimal netAmount) {
		this.netAmount = netAmount;
	}

	public BigDecimal getReceivedAmount() {
		return receivedAmount;
	}

	public void setReceivedAmount(BigDecimal receivedAmount) {
		this.receivedAmount = receivedAmount;
	}

	public int getVehicleId() {
		return vehicleId;
	}

	public void setVehicleId(int vehicleId) {
		this.vehicleId = vehicleId;
	}

	public int getDriverId() {
		return driverId;
	}

	public void setDriverId(int driverId) {
		this.driverId = driverId;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public int getDeliveredBy() {
		return deliveredBy;
	}

	public void setDeliveredBy(int deliveredBy) {
		this.deliveredBy = deliveredBy;
	}

	public Date getDeliveredDate() {
		return deliveredDate;
	}

	public void setDeliveredDate(Date deliveredDate) {
		this.deliveredDate = deliveredDate;
	}

	public int getAmountReceivedBy() {
		return amountReceivedBy;
	}

	public void setAmountReceivedBy(int amountReceivedBy) {
		this.amountReceivedBy = amountReceivedBy;
	}

	public Date getAmountReceivedDate() {
		return amountReceivedDate;
	}

	public void setAmountReceivedDate(Date amountReceivedDate) {
		this.amountReceivedDate = amountReceivedDate;
	}

	public int getDeliveryStatus() {
		return deliveryStatus;
	}

	public void setDeliveryStatus(int deliveryStatus) {
		this.deliveryStatus = deliveryStatus;
	}

	public int getModeOfPayment() {
		return modeOfPayment;
	}

	public void setModeOfPayment(int modeOfPayment) {
		this.modeOfPayment = modeOfPayment;
	}

	public boolean isDelivered() {
		return isDelivered;
	}

	public void setDelivered(boolean isDelivered) {
		this.isDelivered = isDelivered;
	}

	public boolean isPaymentRecevied() {
		return isPaymentRecevied;
	}

	public void setPaymentRecevied(boolean isPaymentRecevied) {
		this.isPaymentRecevied = isPaymentRecevied;
	}

	public int getdCAmountCheckBy() {
		return dCAmountCheckBy;
	}

	public void setdCAmountCheckBy(int dCAmountCheckBy) {
		this.dCAmountCheckBy = dCAmountCheckBy;
	}

	public Date getdCAmountCheckDate() {
		return dCAmountCheckDate;
	}

	public void setdCAmountCheckDate(Date dCAmountCheckDate) {
		this.dCAmountCheckDate = dCAmountCheckDate;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getAllocatedDriverName() {
		return allocatedDriverName;
	}

	public void setAllocatedDriverName(String allocatedDriverName) {
		this.allocatedDriverName = allocatedDriverName;
	}

	public boolean isOfficeDelivery() {
		return officeDelivery;
	}

	public void setOfficeDelivery(boolean officeDelivery) {
		this.officeDelivery = officeDelivery;
	}

	public boolean isOfficeDelivered() {
		return officeDelivered;
	}

	public void setOfficeDelivered(boolean officeDelivered) {
		this.officeDelivered = officeDelivered;
	}

	public Date getOfficeDeliveredDate() {
		return officeDeliveredDate;
	}

	public void setOfficeDeliveredDate(Date officeDeliveredDate) {
		this.officeDeliveredDate = officeDeliveredDate;
	}

	public int getOfficeId() {
		return officeId;
	}

	public void setOfficeId(int officeId) {
		this.officeId = officeId;
	}

	public int getRowStatus() {
		return rowStatus;
	}

	public void setRowStatus(int rowStatus) {
		this.rowStatus = rowStatus;
	}

	public String getDeliveryByName() {
		return deliveryByName;
	}

	public void setDeliveryByName(String deliveryByName) {
		this.deliveryByName = deliveryByName;
	}

	public String getAmountReceivedByName() {
		return amountReceivedByName;
	}

	public void setAmountReceivedByName(String amountReceivedByName) {
		this.amountReceivedByName = amountReceivedByName;
	}
	
}
