package com.arasu.billing.core.master.repository;

import java.util.List;
import java.util.Map;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.arasu.billing.core.master.entity.BranchMaster;

public interface BranchMasterRepository extends JpaRepository<BranchMaster, Integer> {

	@Query("SELECT branch.id as id,branch.branchName as branchName,branch.printerName as printerName FROM BranchMaster branch WHERE branch.rowStatus=1 ")
	List<Map<String, Object>> getAllBranchs();

}
