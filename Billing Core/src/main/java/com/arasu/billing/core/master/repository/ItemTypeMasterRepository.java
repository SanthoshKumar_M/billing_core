package com.arasu.billing.core.master.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.arasu.billing.core.master.entity.ItemTypeMaster;

@Repository
public interface ItemTypeMasterRepository extends JpaRepository<ItemTypeMaster, Integer> {

}
