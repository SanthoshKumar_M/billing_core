package com.arasu.billing.core.master.repository;

import java.util.List;
import java.util.Map;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.arasu.billing.core.master.entity.ItemMaster;

@Repository
public interface ItemMasterRepository extends JpaRepository<ItemMaster, Integer> {
	@Query("SELECT item.id as id,item.itemName as itemName,item.rowStatus as rowStatus FROM ItemMaster item WHERE item.itemName like :itemName% and item.rowStatus=1")
	List<Map<String, Object>> itemNameSuggestion(String itemName);

	@Query("SELECT item.id as id,item.itemName as itemName,item.itemPrintName as itemPrintName,"
			+ "item.itemTypeMaster.id as itemTypeMasterId,item.rowStatus as rowStatus,item.createUser.id as createUserId,"
			+ "item.createDateTime as createDateTime FROM ItemMaster item left outer join item.itemTypeMaster left outer join item.createUser WHERE item.id= :id ")
	List<Map<String, Object>> getItemDetailsById(int id);

	boolean existsByItemName(String itemName);

	@Query("SELECT item.id as id,item.itemName as itemName,item.itemPrintName as itemPrintName ,item.itemTypeMaster.id as itemTypeMasterId,item.rowStatus as rowStatus FROM ItemMaster item WHERE item.rowStatus=1")
	List<Map<String, Object>> getAllItems();

}
