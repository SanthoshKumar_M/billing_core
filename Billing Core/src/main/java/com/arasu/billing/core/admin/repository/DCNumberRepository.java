package com.arasu.billing.core.admin.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.arasu.billing.core.admin.entity.DCNumber;
import com.arasu.billing.core.master.entity.BranchMaster;

public interface DCNumberRepository extends JpaRepository<DCNumber, Integer> {

	DCNumber findTopByOrderByIdDesc();

	DCNumber findFirstByBranchMasterOrderByIdDesc(BranchMaster branchMaster);

	/*
	 * @Query("SELECT dcNumber.dcNumber FROM DCNumber dcNumber where dcNumber.branchMaster.id=:branchMasterId ORDER BY dcNumber.id DESC"
	 * ) Integer findTopByOrderByIdDesc(int branchMasterId);
	 */
}
