package com.arasu.billing.core.master.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.arasu.billing.core.common.response.APIResponse;
import com.arasu.billing.core.master.entity.CustomerMaster;
import com.arasu.billing.core.master.service.CustomerMasterService;

@RestController
public class CustomerMasterController {
	@Autowired
	private CustomerMasterService customerMasterService;

	@GetMapping("/checkCustomerIsAvailable/{customerName}")
	public APIResponse checkCustomerIsAvailable(@PathVariable String customerName) {
		return customerMasterService.checkCustomerIsAvailable(customerName);
	}

	@GetMapping("/getCustomersDetailsById/{id}")
	public APIResponse findCustomerById(@PathVariable int id) {
		return customerMasterService.getCustomerDetailsById(id);
	}

	@GetMapping("/getAllCustomerDetails")
	public APIResponse findAllCustomer() {
		return customerMasterService.getAllCustomerDetails();
	}

	@PostMapping("/saveCustomerDetails")
	public APIResponse saveCustomerDetails(@RequestBody CustomerMaster customerMaster) {
		return customerMasterService.saveCustomerDetails(customerMaster);
	}

	@PutMapping("/updateCustomerDetails")
	public APIResponse updateCustomerDetails(@RequestBody CustomerMaster customerMaster) {
		return customerMasterService.updateCustomerDetails(customerMaster);
	}

	@GetMapping("/getCustomersBySuggestionName/{customerName}")
	public APIResponse customerNameSuggestion(@PathVariable String customerName) {
		return customerMasterService.customerNameSuggestion(customerName);
	}

	@PostMapping("/getAllNewCustomerDetails")
	public APIResponse findNewAllCustomer(@RequestBody List<Integer> avaiableCustomersIds) {
		return customerMasterService.getAllNewCustomerDetails(avaiableCustomersIds);
	}
}
