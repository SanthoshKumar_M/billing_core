package com.arasu.billing.core.master.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.arasu.billing.core.common.data.VehicleData;
import com.arasu.billing.core.common.response.APIResponse;
import com.arasu.billing.core.master.entity.EmployeeMaster;
import com.arasu.billing.core.master.entity.VehicleInsuranceDetailsMaster;
import com.arasu.billing.core.master.entity.VehicleMaster;
import com.arasu.billing.core.master.repository.VehicleInsuranceDetailsRepository;
import com.arasu.billing.core.master.repository.VehicleMasterRepository;

@Service
public class VehicleMasterService {
	@Autowired
	private VehicleMasterRepository vehicleMasterRepository;

	@Autowired
	private VehicleInsuranceDetailsRepository vehicleInsuranceDetailsRepository;

	public APIResponse checkVehicleIsAvailable(String vehicleNumber) {
		APIResponse apiResponse = new APIResponse();
		apiResponse.setData(vehicleMasterRepository.existsByvehicleNumber(vehicleNumber));
		return apiResponse;
	}

	public APIResponse getAllVehicleDetails() {
		APIResponse apiResponse = new APIResponse();
//		List<Map<String, Object>> searchResult = vehicleMasterRepository.getAllVehicle();
		List<VehicleMaster> vehicleMasters = vehicleMasterRepository.findAll();
//		for (int i = 0; i < searchResult.size(); i++) {
//			vehicleMasters.add(convertMapTOVehicleMaster(searchResult.get(i)));
//		}
		apiResponse.setData(vehicleMasters);
		apiResponse.setError(null != apiResponse.getData() ? null : "No Vehicle Found");
		return apiResponse;
	}

	public APIResponse vehicleNumberSuggestion(String vehicleNumber) {
		APIResponse apiResponse = new APIResponse();
		List<Map<String, Object>> searchResult = vehicleMasterRepository.vehicleNumberSuggestion(vehicleNumber);
		List<VehicleMaster> vehicleMasters = new ArrayList<>();
		for (int i = 0; i < searchResult.size(); i++) {
			vehicleMasters.add(convertMapTOVehicleMaster(searchResult.get(i)));
		}
		apiResponse.setData(vehicleMasters);
		apiResponse.setError(null != apiResponse.getData() ? null : "Vehicle Number Suggestion Null");
		return apiResponse;
	}

	public APIResponse getVehicleMasterDetailsById(int id) {
		APIResponse apiResponse = new APIResponse();
		List<Map<String, Object>> searchResult = vehicleInsuranceDetailsRepository.getvehicleMasterDetailsById(id);
		apiResponse.setData(convertSuggestionItemList(searchResult));
		apiResponse.setError(null != apiResponse.getData() ? null : "Vehicle Number Null");
		return apiResponse;
	}

	@Transactional(rollbackFor = RuntimeException.class)
	public APIResponse saveVehicleMasterDetails(VehicleData vehicleData) {
		APIResponse apiResponse = new APIResponse();
		VehicleData data = new VehicleData();
		VehicleMaster vehicleMaster = vehicleData.getVehicleMaster();
		vehicleMaster.setCreateDateTime(new Date());
		data.setVehicleMaster(vehicleMasterRepository.save(vehicleMaster));
		vehicleData.getInsuranceDetails().forEach(vehicleInsurance -> {
			vehicleInsurance.setVehicleMaster(vehicleMaster);
			vehicleInsurance.setCreateDateTime(new Date());
		});
		data.setInsuranceDetails(vehicleInsuranceDetailsRepository.saveAll(vehicleData.getInsuranceDetails()));
		apiResponse.setData(data);
		apiResponse.setError(null != apiResponse.getData() ? null : "Vehicle insert Null");
		return apiResponse;
	}

	@Transactional(rollbackFor = RuntimeException.class)
	public APIResponse updateVehicleMasterDetails(VehicleData vehicleData) {
		VehicleData data = new VehicleData();
		APIResponse apiResponse = new APIResponse();
		VehicleMaster vehicleMaster = vehicleData.getVehicleMaster();
		vehicleMaster.setUpdateDateTime(new Date());
		data.setVehicleMaster(vehicleMasterRepository.save(vehicleMaster));
		vehicleData.getInsuranceDetails().forEach(vehicleInsurance -> {
			if (vehicleInsurance.getId() > 0) {
				vehicleInsurance.setUpdateDateTime(new Date());
			} else if (vehicleInsurance.getId() == 0) {
				vehicleInsurance.setCreateDateTime(new Date());
			}
			vehicleInsurance.setVehicleMaster(vehicleMaster);
		});
		data.setInsuranceDetails(vehicleInsuranceDetailsRepository.saveAll(vehicleData.getInsuranceDetails()));
		apiResponse.setData(data);
		apiResponse.setError(null != apiResponse.getData() ? null : "vehicle Master Update in Issue");
		return apiResponse;
	}

	private VehicleMaster convertMapTOVehicleMaster(Map<String, Object> map) {
		VehicleMaster vehicleMaster = new VehicleMaster();
		if (null != map) {
			vehicleMaster.setId(Integer.parseInt(map.get("id").toString()));
			vehicleMaster.setVehicleNumber(map.get("vehicleNumber").toString());
			vehicleMaster.setVehicleModel(null != map.get("vehicleModel") ? Integer.parseInt(map.get("vehicleModel").toString()) : 0);
			vehicleMaster.setVehicleMakeName(null != map.get("vehicleMakeName") ? map.get("vehicleMakeName").toString() : "");
			vehicleMaster.setVehicleOwnerName(null != map.get("vehicleOwnerName") ? map.get("vehicleOwnerName").toString() : "");
			vehicleMaster.setFcDate(null != map.get("fcDate") ? (Date) map.get("fcDate") : null);
			vehicleMaster.setTax(null != map.get("tax") ? (Date) map.get("tax") : null);
			vehicleMaster.setPermit(null != map.get("permit") ? (Date) map.get("permit") : null);
			vehicleMaster.setRowStatus(Integer.parseInt(map.get("rowStatus").toString()));
			if (null != map.get("createUserId")) {
				EmployeeMaster employeeMaster = new EmployeeMaster();
				employeeMaster.setId(Integer.parseInt(map.get("createUserId").toString()));
				vehicleMaster.setCreateUser(employeeMaster);
				vehicleMaster.setCreateDateTime((Date) map.get("createDateTime"));
			}
		}
		return vehicleMaster;
	}

	private VehicleData convertSuggestionItemList(final List<Map<String, Object>> searchResult) {
		VehicleData data = new VehicleData();
		List<VehicleInsuranceDetailsMaster> insuranceDetails = new ArrayList<>();
		if (null != searchResult) {
			for (int i = 0; i < searchResult.size(); i++) {
				VehicleInsuranceDetailsMaster insuranceDetail = new VehicleInsuranceDetailsMaster();
				Map<String, Object> map = searchResult.get(i);
				if (i == 0) {
					data.setVehicleMaster(convertMapTOVehicleMaster(searchResult.get(i)));
				}
				insuranceDetail.setId(Integer.parseInt(map.get("vehicleInsuranceId").toString()));
				insuranceDetail.setInsuranceNumber(map.get("vehicleInsuranceNumber").toString());
				insuranceDetail.setFromDate((Date) map.get("vehicleInsuranceFromDate"));
				insuranceDetail.setToDate((Date) map.get("vehicleInsuranceToDate"));
				insuranceDetail.setRowStatus(Integer.parseInt(map.get("vehicleInsuranceRowStatus").toString()));

				if (null != map.get("vehicleInsuranceCreateUserId")) {
					EmployeeMaster employeeMaster = new EmployeeMaster();
					employeeMaster.setId(Integer.parseInt(map.get("vehicleInsuranceCreateUserId").toString()));
					insuranceDetail.setCreateUser(employeeMaster);
					insuranceDetail.setCreateDateTime((Date) map.get("vehicleInsuranceCreateDateTime"));
				}
				insuranceDetails.add(insuranceDetail);
			}
		}
		data.setInsuranceDetails(insuranceDetails);
		return data;
	}

}
