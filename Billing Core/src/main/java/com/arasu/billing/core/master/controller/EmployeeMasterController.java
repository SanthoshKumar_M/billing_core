package com.arasu.billing.core.master.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.arasu.billing.core.common.response.APIResponse;
import com.arasu.billing.core.master.service.EmployeeMasterService;

@RestController
public class EmployeeMasterController {
	@Autowired
	private EmployeeMasterService employeeMasterService;

	@GetMapping("/getAllEmployeeDetails")
	public APIResponse findAllEmployee() {
		return employeeMasterService.getAllEmployeeDetails();
	}

	@GetMapping("/getAllEmployeeType")
	public APIResponse findAllEmployeeType() {
		return employeeMasterService.getAllEmployeeType();
	}

	@PostMapping("/saveEmployeeDetails")
	public APIResponse saveEmployeeDetails(@RequestBody Map<String, Object> employeeMap) {
		return employeeMasterService.saveEmployeeDetails(employeeMap);
	}

	@PutMapping("/updateEmployeeDetails")
	public APIResponse updateCustomerDetails(@RequestBody Map<String, Object> employeeMap) throws Exception {
		return employeeMasterService.updateEmployeeDetails(employeeMap);
	}

	@GetMapping("/getEmployeeBySuggestionName/{employeeName}")
	public APIResponse EmployeeNameSuggestion(@PathVariable String employeeName) {
		return employeeMasterService.employeeNameSuggestion(employeeName);
	}

	@GetMapping("/checkEmployeeIsAvailable/{employeeName}")
	public APIResponse checkEmployeeIsAvailable(@PathVariable String employeeName) {
		return employeeMasterService.checkEmployeeIsAvailable(employeeName);
	}

	@GetMapping("/getEmployeeDetailsById/{id}")
	public APIResponse findEmployeeById(@PathVariable int id) {
		return employeeMasterService.getEmployeeDetailsById(id);
	}

}
