package com.arasu.billing.core.master.entity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.arasu.billing.core.common.to.BaseEntity;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@Entity
@Table(name = "state_master")
public class StateMaster extends BaseEntity {
	@Column(name = "state_name")
	private String stateName;

	@JsonInclude(value = Include.NON_NULL)
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "country_master_id")
	private CountryMaster countryMaster;

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public CountryMaster getCountryMaster() {
		return countryMaster;
	}

	public void setCountryMaster(CountryMaster countryMaster) {
		this.countryMaster = countryMaster;
	}

}
