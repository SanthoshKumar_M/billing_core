package com.arasu.billing.core.common;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class DateUtil {
	static DateFormat dateMonthYearDateFormat = new SimpleDateFormat("dd-MM-yyyy");

	public DateUtil() {
		// TODO Auto-generated constructor stub
	}

	public static DateFormat getDateMonthYearDateFormat() {
		return dateMonthYearDateFormat;
	}

	public static void setDateMonthYearDateFormat(DateFormat dateMonthYearDateFormat) {
		DateUtil.dateMonthYearDateFormat = dateMonthYearDateFormat;
	}

}
