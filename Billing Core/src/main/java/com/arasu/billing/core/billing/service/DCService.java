package com.arasu.billing.core.billing.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.arasu.billing.core.admin.entity.DCNumber;
import com.arasu.billing.core.admin.service.DCNumberService;
import com.arasu.billing.core.billing.entity.DC;
import com.arasu.billing.core.billing.entity.DCLine;
import com.arasu.billing.core.billing.repository.DCLineRepository;
import com.arasu.billing.core.billing.repository.DCRepository;
import com.arasu.billing.core.common.DateUtil;
import com.arasu.billing.core.common.constant.BillingConstant;
import com.arasu.billing.core.common.data.DCData;
import com.arasu.billing.core.common.data.DCListTO;
import com.arasu.billing.core.common.exception.BillingException;
import com.arasu.billing.core.common.response.APIResponse;
import com.arasu.billing.core.common.to.ReportTO;
import com.arasu.billing.core.master.entity.BranchMaster;
import com.arasu.billing.core.master.entity.CityMaster;
import com.arasu.billing.core.master.entity.CustomerMaster;
import com.arasu.billing.core.master.entity.EmployeeMaster;
import com.arasu.billing.core.master.entity.ItemMaster;
import com.arasu.billing.core.master.repository.CustomerMasterRepository;
import com.arasu.billing.core.print.GeneratePDFFile;
import com.arasu.billing.core.report.repositoryimp.DCReportRepositoryImp;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class DCService {
	
	ObjectMapper mapper = new ObjectMapper();
	GeneratePDFFile file = new GeneratePDFFile();
	@Autowired
	private CustomerMasterRepository customerMasterRepository;

	@Autowired
	private DCRepository dcRepository;

	@Autowired
	private DCLineRepository dcLineRepository;

	@Autowired
	private DCNumberService dcNumberService;

//	@Autowired
//	private ReportGenerator reportGenerator;
	
	@Autowired
	DCReportRepositoryImp dcReportRepositoryImp;

	private int count = 0;

	@Transactional(rollbackFor = RuntimeException.class)
	public APIResponse saveDCDetails(DCData dcData) {
		APIResponse apiResponse = new APIResponse();
		DCData responceDCData = new DCData();
		DCNumber dcNumber;

		createNewCustomerViaDCCreation(dcData);

		synchronized (DCNumberService.class) {
			dcNumber = dcNumberService.getDCRunningNumber(dcData.getDc().getBranchMaster());
		}
		dcData.getDc().setCreateDateTime(new Date());
		dcData.getDc().setdCNo(dcNumber.getDcNumber());
		
		responceDCData.setDc(dcRepository.save(dcData.getDc()));
		List<DCLine> dclineList = dcData.getDcLine().stream().filter(dcline -> dcline.getItemMaster() != null)
				.collect(Collectors.toList());

		dclineList.forEach(dcline -> {
			dcline.setCreateDateTime(new Date());
			dcline.setDc(responceDCData.getDc());
		});

		responceDCData.setDcLine(dcLineRepository.saveAll(dclineList));

		generateReport(dcData, responceDCData);
		apiResponse.setData(responceDCData);
//		file.generatePDFFile(responceDCData);
		return apiResponse;
	}

	private void createNewCustomerViaDCCreation(DCData dcData) {
		try {
			if (null != dcData.getDc().getFromCustomerMaster() && dcData.getDc().getFromCustomerMaster().getId() == 0
					&& !dcData.getDc().getFromCustomerMaster().getCustomerName().isEmpty() && dcData.getDc()
							.getFromCustomerMaster().getCustomerAddress().equals(BillingConstant.NEW_CUSTOMER)) {
				dcData.getDc().getFromCustomerMaster().setCreateDateTime(new Date());
				dcData.getDc()
						.setFromCustomerMaster(customerMasterRepository.save(dcData.getDc().getFromCustomerMaster()));
			}
		} catch (HibernateException hibernateException) {
			throw new BillingException(
					"From Customer " + hibernateException.getCause().getCause().getLocalizedMessage());
		} catch (Exception exception) {
			throw new BillingException("From Customer " + exception.getCause().getCause().getLocalizedMessage());
		}
		try {
			if (dcData.getDc().getToCustomerMaster().getId() == 0
					&& !dcData.getDc().getToCustomerMaster().getCustomerName().isEmpty()
					&& dcData.getDc().getToCustomerMaster().getCustomerAddress().equals(BillingConstant.NEW_CUSTOMER)) {
				dcData.getDc().getToCustomerMaster().setCreateDateTime(new Date());
				dcData.getDc().getToCustomerMaster().setMobileNumber(dcData.getDc().getToCustomerMobileNumber());
				dcData.getDc().setToCustomerMaster(customerMasterRepository.save(dcData.getDc().getToCustomerMaster()));
			}
		} catch (HibernateException hibernateException) {
			throw new BillingException(
					"To Customer " + hibernateException.getCause().getCause().getLocalizedMessage());
		} catch (Exception exception) {
			throw new BillingException("To Customer " + exception.getCause().getCause().getLocalizedMessage());
		}

	}

	@Transactional(rollbackFor = RuntimeException.class)
	public APIResponse updateDCDetails(DCData dcData) {
		APIResponse apiResponse = new APIResponse();
		DCData responceDCData = new DCData();
		createNewCustomerViaDCCreation(dcData);
		dcData.getDc().setUpdateDateTime(new Date());
		responceDCData.setDc(dcRepository.save(dcData.getDc()));
		List<DCLine> dclineList = dcData.getDcLine().stream().filter(dcline -> dcline.getItemMaster() != null)
				.collect(Collectors.toList());

		dclineList.forEach(dcline -> {
			if (dcline.getId() > 0) {
				dcline.setUpdateDateTime(new Date());
			} else if (dcline.getId() == 0) {
				dcline.setCreateDateTime(new Date());
			}
			dcline.setDc(responceDCData.getDc());
		});

		responceDCData.setDcLine(dcLineRepository.saveAll(dclineList));
		dcLineRepository.updateRemovedDCLineRowStatus(dcData.getRemoveItemIds(), BillingConstant.DELETED);
		dcData.setDcLine(responceDCData.getDcLine());
		generateReport(responceDCData, responceDCData);
		apiResponse.setData(responceDCData);
//		file.generatePDFFile(responceDCData);
		return apiResponse;
	}

	private void generateReport(DCData dcData, DCData responceDCData) {
		DC dc = dcData.getDc();
		List<ReportTO> reportTOs = new ArrayList<ReportTO>();
		Map<String, Object> parameters = new HashMap<>();
		parameters.put("id", dc.getId());
		parameters.put("dCNO", dc.getdCNo());
		parameters.put("dCDate", DateUtil.getDateMonthYearDateFormat().format(dc.getdCDate()));
		parameters.put("fromCustomerName",
				null != dc.getFromCustomerMaster() ? dc.getFromCustomerMaster().getCustomerName() : "");
		parameters.put("fromCustomerMobileNumber",
				null != dc.getFromCustomerMaster() ? dc.getFromCustomerMaster().getMobileNumber() : "");
		parameters.put("fromCustomerCity",
				null != dc.getFromCustomerMaster() ? dc.getFromCustomerMaster().getCityMaster().getCityName() : "");
		parameters.put("toCustomerName", dc.getToCustomerMaster().getCustomerPrintName());
		parameters.put("toCustomerCity", dc.getToCustomerMaster().getCityMaster().getCityName());
		parameters.put("toCustomerMobileNumber", dc.getToCustomerMobileNumber());

		parameters.put("freightCharge", dc.getAmount().setScale(2));
		parameters.put("otherChargeDetails", dc.getOtherChargesDetails());
		parameters.put("otherCharges", dc.getOtherChargesAmount().setScale(2));
		parameters.put("totalQty", dc.getTotalNoOfQty());
		parameters.put("totalAmount", dc.getNetAmount().setScale(2));
		parameters.put("paymentMode", dc.getModeOfPay());
		List<DCLine> dclineList = dcData.getDcLine().stream().filter(dcline -> dcline.getItemMaster() != null)
				.collect(Collectors.toList());

		dclineList.forEach(dcline -> {
			ReportTO reportTO = new ReportTO();
			reportTO.setItemName(dcline.getItemMaster().getItemPrintName());
			reportTO.setQty(dcline.getQty());
			reportTOs.add(reportTO);
		});
		responceDCData.setParameters(parameters);
		responceDCData.setReportTOs(reportTOs);

//		reportGenerator.exportReport(dc.getdCNo() + "-" + DateUtil.getDateMonthYearDateFormat().format(dc.getdCDate()), parameters, reportTOs);

	}
	
	@Transactional(rollbackFor = RuntimeException.class)
	public APIResponse updateDCAllocations(List<DCListTO> dcListTO) {
		APIResponse apiResponse = new APIResponse();
		count = 0;
		dcListTO.forEach(dcline -> {
			EmployeeMaster amountRecivedBy = null;
			if (dcline.getAmountReceivedBy() > 0) {
				amountRecivedBy = new EmployeeMaster();
				amountRecivedBy.setId(dcline.getAmountReceivedBy());
			}
			int result = dcRepository.updateDriverAndVehicleDetails(dcline.getId(), dcline.getAmount(),
					dcline.getNetAmount(), dcline.getDriverId(), dcline.getVehicleId(), amountRecivedBy,
					dcline.isOfficeDelivery(),dcline.getOfficeId());
			count = count + result;
		});
		apiResponse.setData(count);
		return apiResponse;
	}

	@Transactional(rollbackFor = RuntimeException.class)
	public APIResponse receivedOfficeItemDetails(List<DCListTO> dcListTO) {
		APIResponse apiResponse = new APIResponse();
		count = 0;
		dcListTO.forEach(dcline -> {
			int result = dcRepository.receivedOfficeItemDetails(dcline.getId(),
					BillingConstant.DELIVERY_STATUS_INOFFICE, Boolean.TRUE, new Date());
			count = count + result;
		});
		apiResponse.setData(count);
		return apiResponse;
	}

	@Transactional(rollbackFor = RuntimeException.class)
	public APIResponse updateDCTransfer(List<DCListTO> dcListTO) {
		APIResponse apiResponse = new APIResponse();
		count = 0;
		dcListTO.forEach(dcline -> {
			EmployeeMaster amountRecivedBy = null;
			if (dcline.getAmountReceivedBy() > 0) {
				amountRecivedBy = new EmployeeMaster();
				amountRecivedBy.setId(dcline.getAmountReceivedBy());
			}
			int result = dcRepository.updateDeliveryandPaymentDriverDetails(dcline.getId(), dcline.getDeliveredBy(),
					amountRecivedBy);
			count = count + result;
		});
		apiResponse.setData(count);
		return apiResponse;
	}

	@Transactional(rollbackFor = RuntimeException.class)
	public APIResponse updateDCCollection(List<DCListTO> dcListTO) {
		APIResponse apiResponse = new APIResponse();
		count = 0;
		dcListTO.forEach(dcline -> {
			int dcStatus = BillingConstant.ACTIVE;
			dcline.setdCAmountCheckDate(new Date());
			EmployeeMaster dCAmountCheckBy = null;
			if (dcline.getdCAmountCheckBy() > 0) {
				dCAmountCheckBy = new EmployeeMaster();
				dCAmountCheckBy.setId(dcline.getdCAmountCheckBy());
			}
			if (!dcline.isDelivered()) {
				dcline.setDeliveryStatus(BillingConstant.DELIVERY_STATUS_DONE);
				dcline.setDeliveredDate(new Date());
			}
			if (dcline.isPaymentRecevied()) {
				dcStatus = BillingConstant.CLOSED;
			} else {
				if (dcline.getModeOfPayment() == BillingConstant.TOPAY
						&& getPaymentStatus(dcline.getReceivedAmount())) {
					dcline.setAmountReceivedDate(new Date());
					dcStatus = BillingConstant.CLOSED;
				}
			}
			int result = dcRepository.updateDCCollectionDetails(dcline.getId(), dcline.getDeliveryStatus(),
					dcline.getDeliveredDate(), dcline.getReceivedAmount(), dcline.getAmountReceivedDate(), dcStatus,
					dCAmountCheckBy, dcline.getAmountReceivedDate());
			count = count + result;
		});
		apiResponse.setData(count);
		return apiResponse;
	}


	public APIResponse getDCList(int branchMasterId) {
		APIResponse apiResponse = new APIResponse();
		apiResponse.setData(convertObjectToList(dcRepository.getDCList(branchMasterId)));
		return apiResponse;
	}

	public APIResponse getDCList(int branchMasterId, int dCNo) {
		APIResponse apiResponse = new APIResponse();
		apiResponse.setData(convertObjectToList(dcRepository.findAllByBranchMasterAndDCNo(branchMasterId, dCNo)));
		return apiResponse;
	}

	public APIResponse getDCDetailsById(int id) {
		APIResponse apiResponse = new APIResponse();
		apiResponse.setData(convertObjectToDCData(dcRepository.getDCDetails(id, BillingConstant.DELETED)));
		return apiResponse;
	}

	public APIResponse getDCList(List<Integer> branchMasterIds, java.sql.Date selectedDate,
			List<Integer> selectedCitIds) {
		APIResponse apiResponse = new APIResponse();
		apiResponse.setData(
				convertObjectToDCListTO(dcLineRepository.getDCDetails(BillingConstant.ACTIVE, BillingConstant.ACTIVE,
						BillingConstant.DELIVERY_STATUS_PENDING, branchMasterIds, selectedDate, selectedCitIds)));
		return apiResponse;
	}

	public APIResponse getOfficeItemDCList(int branchMasterId) {
		APIResponse apiResponse = new APIResponse();
		apiResponse.setData(
				convertObjectToDCListTO(dcLineRepository.getOfficeItemDCList(BillingConstant.ACTIVE, branchMasterId)));
		return apiResponse;
	}
	
	public APIResponse getReceivedOfficeItemDCList(int branchId) {
		APIResponse apiResponse = new APIResponse();
		apiResponse.setData(convertObjectToDCListTO(dcLineRepository.getReceivedOfficeItemDCList(BillingConstant.ACTIVE,
				BillingConstant.ACTIVE, BillingConstant.DELIVERY_STATUS_INOFFICE, branchId)));
		return apiResponse;
	}
	
	public APIResponse getDriverDCList(int selectedDriverId) {
		APIResponse apiResponse = new APIResponse();
		apiResponse.setData(convertObjectToDCListTO(dcLineRepository.getDriverDCList(BillingConstant.ACTIVE,
				BillingConstant.ACTIVE, BillingConstant.DELIVERY_STATUS_PENDING,BillingConstant.DELIVERY_STATUS_INOFFICE, selectedDriverId)));
		return apiResponse;
	}
	
	public APIResponse getMasterReport(java.sql.Date fromDate, java.sql.Date toDate, int customerMasterId,int cityMasterId, int driverId) {
		APIResponse apiResponse = new APIResponse();
//		apiResponse.setData(convertObjectToDCData(dcRepository.getMasterReport(BillingConstant.DELETED,fromDate, toDate, customerMasterId, cityMasterId, driverId)));
		apiResponse.setData(convertObjectToDCListTO(dcReportRepositoryImp.getDCDetails(fromDate, toDate, customerMasterId, cityMasterId, driverId)));
		return apiResponse;
	}


	public APIResponse getMonthlyReport(java.sql.Date fromDate, java.sql.Date toDate, int customerMasterId) {
		APIResponse apiResponse = new APIResponse();
		apiResponse.setData(convertObjectToDCListTO(dcLineRepository.getMonthlyReport(BillingConstant.ACTIVE,
				BillingConstant.ACTIVE, fromDate, toDate, customerMasterId, BillingConstant.PAID)));
		return apiResponse;
	}

	
	private List<DC> convertObjectToList(final List<Map<String, Object>> searchResult) {
		List<DC> dcs = new ArrayList<>();
		if (null != searchResult) {
			for (int i = 0; i < searchResult.size(); i++) {
				DC dc = new DC();
				Map<String, Object> map = searchResult.get(i);
				dc.setId(Integer.parseInt(map.get("id").toString()));
				dc.setdCNo(Integer.parseInt(map.get("dCNo").toString()));
				dc.setdCDate((Date) map.get("dCDate"));
				dc.setRemarks(
						null != map.get("remarks") ? map.get("remarks").toString() : BillingConstant.EMPTY_STRING);
				dc.setRowStatus(Integer.parseInt(map.get("rowStatus").toString()));
				dc.setDeliveryStatus(Integer.parseInt(map.get("deliveryStatus").toString()));
				dc.setReceivedAmount(new BigDecimal(map.get("receivedAmount").toString())
						.setScale(BillingConstant.TWO_DECIMAL_PLACE));

				if (null != map.get("fromCustomerMasterId")) {
					CustomerMaster fromCustomer = new CustomerMaster();
					fromCustomer.setId(Integer.parseInt(map.get("fromCustomerMasterId").toString()));
					fromCustomer.setCustomerName(map.get("fromCustomerMasterName").toString());
					dc.setFromCustomerMaster(fromCustomer);
				}

				if (null != map.get("toCustomerMasterId")) {
					CustomerMaster toCustomer = new CustomerMaster();
					toCustomer.setId(Integer.parseInt(map.get("toCustomerMasterId").toString()));
					toCustomer.setCustomerName(map.get("toCustomerMasterName").toString());
					toCustomer.setMobileNumber(null != map.get("mobileNumber") ? map.get("mobileNumber").toString()
							: BillingConstant.EMPTY_STRING);
					dc.setToCustomerMaster(toCustomer);
				}

				if (null != map.get("createUserId")) {
					EmployeeMaster employeeMaster = new EmployeeMaster();
					employeeMaster.setId(Integer.parseInt(map.get("createUserId").toString()));
					employeeMaster.setEmployeeName(null != map.get("createUserName") ? map.get("createUserName").toString() : "");
					dc.setCreateUser(employeeMaster);
					dc.setCreateDateTime((Date) map.get("createDateTime"));

				}
				dcs.add(dc);
			}
		}

		return dcs;

	}

	private DCData convertObjectToDCData(final List<Map<String, Object>> searchResult) {
		DC dc = new DC();
		List<DCLine> dcLines = new ArrayList<>();
		DCData data = new DCData();
		if (null != searchResult) {
			for (int i = 0; i < searchResult.size(); i++) {
				Map<String, Object> map = searchResult.get(i);
				DCLine dcLine = new DCLine();
				if (i == 0) {
					dc.setId(Integer.parseInt(map.get("id").toString()));
					dc.setdCNo(Integer.parseInt(map.get("dCNo").toString()));
					dc.setdCDate((Date) map.get("dCDate"));
					dc.setToCustomerMobileNumber(map.get("toCustomerMobileNumber").toString());
					dc.setRemarks(
							null != map.get("remarks") ? map.get("remarks").toString() : BillingConstant.EMPTY_STRING);
					dc.setAmount(
							new BigDecimal(map.get("amount").toString()).setScale(BillingConstant.TWO_DECIMAL_PLACE));
					dc.setOtherChargesDetails(map.get("otherChargesDetails").toString());
					dc.setOtherChargesAmount(new BigDecimal(map.get("otherChargesAmount").toString())
							.setScale(BillingConstant.TWO_DECIMAL_PLACE));
					dc.setModeOfPay(Integer.parseInt(map.get("modeOfPay").toString()));
					dc.setRowStatus(Integer.parseInt(map.get("rowStatus").toString()));
					dc.setDeliveryStatus(Integer.parseInt(map.get("deliveryStatus").toString()));
					dc.setReceivedAmount(new BigDecimal(map.get("receivedAmount").toString()));
					dc.setOfficeId(Integer.parseInt(null != map.get("officeId") ? map.get("officeId").toString()
							: BillingConstant.ZERO.toString()));
					dc.setOfficeDelivery(Boolean.parseBoolean(map.get("officeDelivery").toString()));
					dc.setOfficeDelivered(Boolean.parseBoolean(map.get("officeDelivered").toString()));
					if (null != map.get("fromCustomerMasterId")) {
						CustomerMaster fromCustomer = new CustomerMaster();
						fromCustomer.setId(Integer.parseInt(map.get("fromCustomerMasterId").toString()));
						fromCustomer.setCustomerName(map.get("fromCustomerMasterName").toString());
						fromCustomer.setCustomerPrintName(map.get("fromCustomerPrintName").toString());
						CityMaster cityMaster = new CityMaster();
						cityMaster.setId(Integer.parseInt(map.get("fromCityMasterId").toString()));
						cityMaster.setCityName(map.get("fromCityMastercityName").toString());
						fromCustomer.setCityMaster(cityMaster);
						dc.setFromCustomerMaster(fromCustomer);
					}

					if (null != map.get("toCustomerMasterId")) {
						CustomerMaster toCustomer = new CustomerMaster();
						toCustomer.setId(Integer.parseInt(map.get("toCustomerMasterId").toString()));
						toCustomer.setCustomerName(map.get("toCustomerMasterName").toString());
						toCustomer.setCustomerPrintName(map.get("toCustomerPrintName").toString());
						toCustomer.setMobileNumber(null != map.get("mobileNumber") ? map.get("mobileNumber").toString()
								: BillingConstant.EMPTY_STRING);
						CityMaster cityMaster = new CityMaster();
						cityMaster.setId(Integer.parseInt(map.get("toCityMasterId").toString()));
						cityMaster.setCityName(map.get("toCityMastercityName").toString());
						toCustomer.setCityMaster(cityMaster);
						dc.setToCustomerMaster(toCustomer);
					}

					BranchMaster branchMaster = new BranchMaster();
					branchMaster.setId(Integer.parseInt(map.get("branchMasterId").toString()));
					dc.setBranchMaster(branchMaster);

					if (null != map.get("createUserId")) {
						EmployeeMaster employeeMaster = new EmployeeMaster();
						employeeMaster.setId(Integer.parseInt(map.get("createUserId").toString()));
						dc.setCreateUser(employeeMaster);
						dc.setCreateDateTime((Date) map.get("createDateTime"));

					}
				}
				dcLine.setId(Integer.parseInt(map.get("dcLineId").toString()));
				ItemMaster itemMaster = new ItemMaster();
				itemMaster.setId(Integer.parseInt(map.get("itemMasterId").toString()));
				itemMaster.setItemName(map.get("itemMasterName").toString());
				dcLine.setItemMaster(itemMaster);
				dcLine.setQty(Integer.parseInt(map.get("itemQty").toString()));
				dcLine.setRowStatus(Integer.parseInt(map.get("dclineRowStatus").toString()));
				if (null != map.get("dcLineCreateUserId")) {
					EmployeeMaster employeeMaster = new EmployeeMaster();
					employeeMaster.setId(Integer.parseInt(map.get("dcLineCreateUserId").toString()));
					dcLine.setCreateUser(employeeMaster);
					dcLine.setCreateDateTime((Date) map.get("dcLineCreateDateTime"));
				}

				dcLines.add(dcLine);
			}
		}
		data.setDc(dc);
		data.setDcLine(dcLines);
		return data;

	}

	private List<DCListTO> convertObjectToDCListTO(final List<Map<String, Object>> searchResult) {
		List<DCListTO> dcs = new ArrayList<>();
		if (null != searchResult) {
			for (int i = 0; i < searchResult.size(); i++) {
				DCListTO dcListTO = new DCListTO();
				Map<String, Object> map = searchResult.get(i);
				dcListTO.setId(Integer.parseInt(map.get("id").toString()));
				dcListTO.setdCNo(Integer.parseInt(map.get("dCNo").toString()));
				dcListTO.setdCDate((Date) map.get("dCDate"));
				dcListTO.setFromCustomerName(
						null != map.get("fromCustomerName") ? map.get("fromCustomerName").toString()
								: BillingConstant.EMPTY_STRING);
				dcListTO.setToCustomerName(map.get("toCustomerName").toString());
				dcListTO.setFromCityName(null != map.get("fromCityName") ? map.get("fromCityName").toString()
						: BillingConstant.EMPTY_STRING);
				dcListTO.setToCityName(map.get("toCityName").toString());
				dcListTO.setItemNames(map.get("itemNames").toString());
				dcListTO.setTotalNoOfQty(new BigDecimal(map.get("totalNoOfQty").toString())
						.setScale(BillingConstant.ZERO_DECIMAL_PLACE));
				dcListTO.setAmount(
						new BigDecimal(map.get("amount").toString()).setScale(BillingConstant.TWO_DECIMAL_PLACE));
				dcListTO.setOtherChargesDetails(map.get("otherChargesDetails").toString());
				dcListTO.setOtherChargesAmount(new BigDecimal(map.get("otherChargesAmount").toString())
						.setScale(BillingConstant.TWO_DECIMAL_PLACE));
				dcListTO.setNetAmount(
						new BigDecimal(map.get("netAmount").toString()).setScale(BillingConstant.TWO_DECIMAL_PLACE));
				dcListTO.setDeliveryStatus(Integer.parseInt(map.get("deliveryStatus").toString()));
				dcListTO.setModeOfPayment(Integer.parseInt(map.get("modeOfPayment").toString()));
				dcListTO.setReceivedAmount(new BigDecimal(map.get("receivedAmount").toString())
						.setScale(BillingConstant.TWO_DECIMAL_PLACE));
				dcListTO.setDeliveredBy(
						null != map.get("deliveredBy") ? Integer.parseInt(map.get("deliveredBy").toString())
								: BillingConstant.ZERO);
				dcListTO.setDeliveredDate(null != map.get("deliveredDate") ? (Date) map.get("deliveredDate") : null);
				dcListTO.setAmountReceivedBy(
						null != map.get("amountReceivedBy") ? Integer.parseInt(map.get("amountReceivedBy").toString())
								: BillingConstant.ZERO);
				dcListTO.setAmountReceivedDate(
						null != map.get("amountReceivedDate") ? (Date) map.get("amountReceivedDate") : null);
				dcListTO.setPaymentRecevied(getPaymentStatus(dcListTO.getReceivedAmount()));
				dcListTO.setDelivered(getDeliveryStatus(dcListTO.getDeliveryStatus()));
				dcListTO.setBranchName(null != map.get("branchName") ? map.get("branchName").toString()
						: BillingConstant.EMPTY_STRING);
				dcListTO.setAllocatedDriverName(
						null != map.get("allocatedDriverName") ? map.get("allocatedDriverName").toString()
								: BillingConstant.EMPTY_STRING);
				dcListTO.setOfficeId(Integer.parseInt(null != map.get("officeId") ? map.get("officeId").toString()
						: BillingConstant.ZERO.toString()));				
				dcListTO.setOfficeDelivery(
						null != map.get("officeDelivery") ? Boolean.parseBoolean(map.get("officeDelivery").toString())
								: Boolean.FALSE);
				dcListTO.setRowStatus(Integer.parseInt(map.get("rowStatus").toString()));
				dcListTO.setDeliveryByName(map.get("deliveryByName").toString());
				dcListTO.setAmountReceivedByName(map.get("amountReceivedByName").toString());
				dcs.add(dcListTO);
			}
		}

		return dcs;

	}

	public boolean getDeliveryStatus(int deliveryStatus) {
		return BillingConstant.DELIVERY_STATUS_DONE == deliveryStatus;
	}

	public boolean getPaymentStatus(BigDecimal receivedAmount) {
		if (null == receivedAmount || receivedAmount.compareTo(new BigDecimal(0)) == 0) {
			return Boolean.FALSE;
		}
		return Boolean.TRUE;
	}

}
