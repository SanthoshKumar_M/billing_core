package com.arasu.billing.core.master.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.arasu.billing.core.common.to.BaseEntity;

@Entity
@Table(name = "company_master")
public class CompanyMaster extends BaseEntity {

	@Column(name = "company_name")
	private String companyName;

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}


}
