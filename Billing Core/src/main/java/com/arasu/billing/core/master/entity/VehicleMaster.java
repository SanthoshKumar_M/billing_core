package com.arasu.billing.core.master.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.arasu.billing.core.common.to.BaseEntity;

@Entity
@Table(name = "vehicle_master")
public class VehicleMaster extends BaseEntity {

	@Column(name = "vehicle_number", unique = true)
	private String vehicleNumber;

	@Column(name = "vehicle_model")
	private int vehicleModel;

	@Column(name = "vehicle_make_name")
	private String vehicleMakeName;

	@Column(name = "vehicle_owner_name")
	private String vehicleOwnerName;

	@Column(name = "fc_date")
	private Date fcDate;

	@Column(name = "tax")
	private Date tax;

	@Column(name = "permit")
	private Date permit;

	public String getVehicleNumber() {
		return vehicleNumber;
	}

	public void setVehicleNumber(String vehicleNumber) {
		this.vehicleNumber = vehicleNumber;
	}

	public int getVehicleModel() {
		return vehicleModel;
	}

	public void setVehicleModel(int vehicleModel) {
		this.vehicleModel = vehicleModel;
	}

	public String getVehicleMakeName() {
		return vehicleMakeName;
	}

	public void setVehicleMakeName(String vehicleMakeName) {
		this.vehicleMakeName = vehicleMakeName;
	}

	public String getVehicleOwnerName() {
		return vehicleOwnerName;
	}

	public void setVehicleOwnerName(String vehicleOwnerName) {
		this.vehicleOwnerName = vehicleOwnerName;
	}

	public Date getFcDate() {
		return fcDate;
	}

	public void setFcDate(Date fcDate) {
		this.fcDate = fcDate;
	}

	public Date getTax() {
		return tax;
	}

	public void setTax(Date tax) {
		this.tax = tax;
	}

	public Date getPermit() {
		return permit;
	}

	public void setPermit(Date permit) {
		this.permit = permit;
	}
	
}
