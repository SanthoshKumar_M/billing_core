package com.arasu.billing.core.common.service;

import java.io.FileNotFoundException;
import java.util.List;
import java.util.Map;

import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.HashPrintServiceAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.PrintServiceAttributeSet;
import javax.print.attribute.standard.Copies;
import javax.print.attribute.standard.PrinterName;

import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import com.arasu.billing.core.print.DCPrint;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPrintServiceExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimplePrintServiceExporterConfiguration;

@Service
public class ReportGenerator {
	DCPrint dcPrint = new DCPrint();

	public <T> void print(String fileName, Map<String, Object> parameters, List<T> data) {
//		String path = "d:";
		try {
			String filePath = ResourceUtils.getFile("classpath:report/dc.jrxml").getAbsolutePath();
			JasperReport jasperReport = JasperCompileManager.compileReport(filePath);
			JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(data);
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, dataSource);
			PrintReportToPrinter(jasperPrint);
//			JasperExportManager.exportReportToPdfFile(jasperPrint, path + "/" + fileName + ".pdf");
		} catch (FileNotFoundException | JRException e) {
			e.printStackTrace();
		}
//		dcPrint.rawprint(fileName);
	}

	private void PrintReportToPrinter(JasperPrint jp) throws JRException {
		PrintServiceAttributeSet printServiceAttributeSet = new HashPrintServiceAttributeSet();
		PrintRequestAttributeSet printRequestAttributeSet = new HashPrintRequestAttributeSet();
		printRequestAttributeSet.add(new Copies(1));
		String printerName = "TVS MSP 250 Star";

		PrinterName printer = new PrinterName(printerName, null); // gets printer
		printServiceAttributeSet.add(printer);

		JRPrintServiceExporter exporter = new JRPrintServiceExporter();
		SimplePrintServiceExporterConfiguration configuration = new SimplePrintServiceExporterConfiguration();
		configuration.setPrintRequestAttributeSet(printRequestAttributeSet);
		configuration.setPrintServiceAttributeSet(printServiceAttributeSet);
		configuration.setDisplayPageDialog(false);
		configuration.setDisplayPrintDialog(false);

		exporter.setExporterInput(new SimpleExporterInput(jp));
		exporter.setConfiguration(configuration);

		exporter.exportReport();
	}
}
