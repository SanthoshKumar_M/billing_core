package com.arasu.billing.core.master.repository;

import java.util.List;
import java.util.Map;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.arasu.billing.core.master.entity.CustomerMaster;

@Repository
public interface CustomerMasterRepository extends JpaRepository<CustomerMaster, Integer> {

	@Query("SELECT customer.id as id,customer.customerName as customerName,customer.customerPrintName as customerPrintName,"
			+ "customer.mobileNumber as mobileNumber,customer.cityMaster.id as cityMasterId,customer.cityMaster.cityName as cityName,"
			+ "customer.createUser.id as createUserId,customer.createUser.employeeName as createUserName,customer.createDateTime as createDateTime "
			+ "FROM CustomerMaster customer WHERE customer.rowStatus=1")
	List<Map<String, Object>> getAllCustomerDetails();

	@Query("SELECT customer.id as id,customer.customerName as customerName,customer.customerPrintName as customerPrintName,customer.mobileNumber as mobileNumber,"
			+ "customer.cityMaster.id as cityMasterId,customer.cityMaster.cityName as cityName "
			+ "FROM CustomerMaster customer WHERE customer.customerName like %:customerName% and customer.rowStatus=1")
	List<Map<String, Object>> customerNameSuggestion(String customerName);

	@Query("SELECT customer.id as id,customer.customerName as customerName,customer.customerPrintName as customerPrintName,customer.customerAddress as customerAddress,"
			+ "customer.cityMaster.id as cityMasterId,customer.cityMaster.cityName as cityName,"
			+ "customer.mobileNumber as mobileNumber,customer.alternativeMobileNumber as alternativeMobileNumber,"
			+ "customer.phoneNumber as phoneNumber,customer.rowStatus as rowStatus,customer.createUser.id as createUserId,customer.createDateTime as createDateTime"
			+ " FROM CustomerMaster customer  left outer join customer.cityMaster left outer join customer.createUser  WHERE customer.id= :id ")
	List<Map<String, Object>> getCustomerDetailsById(int id);

	boolean existsByCustomerName(String customerName);

	@Query("SELECT customer.id as id,customer.customerName as customerName,customer.customerPrintName as customerPrintName,customer.mobileNumber as mobileNumber,"
			+ "customer.cityMaster.id as cityMasterId,customer.cityMaster.cityName as cityName FROM CustomerMaster customer WHERE customer.rowStatus=1 and customer.id NOT IN(:avaiableCustomersIds)")
	List<Map<String, Object>> getAllNewCustomerDetails(List<Integer> avaiableCustomersIds);

}
