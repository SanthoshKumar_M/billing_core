package com.arasu.billing.core.master.repository;

import java.util.List;
import java.util.Map;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.arasu.billing.core.master.entity.CityMaster;

public interface CityMasterRepository extends JpaRepository<CityMaster, Integer> {
	@Query("SELECT city.id as id,city.cityName as cityName FROM CityMaster city WHERE city.rowStatus=1 ")
	List<Map<String, Object>> getAllCity();

}
