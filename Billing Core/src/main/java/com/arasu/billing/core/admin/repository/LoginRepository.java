package com.arasu.billing.core.admin.repository;

import java.util.List;
import java.util.Map;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.arasu.billing.core.admin.entity.Login;

public interface LoginRepository extends JpaRepository<Login, Integer> {

	@Query("SELECT login FROM Login login WHERE login.userName =:userName and login.password=:password and login.activeUser =1")
	Login findLoginUser(String userName, String password);

	@Query("SELECT login.id as id,login.userName as userName,login.password as password,"
			+ "login.activeUser as activeUser FROM Login login WHERE login.employeeMaster.id =:employeeId")
	List<Map<String, Object>> findByEmployeeId(int employeeId);
}
