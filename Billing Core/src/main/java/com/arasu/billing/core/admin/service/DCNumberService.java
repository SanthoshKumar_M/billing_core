package com.arasu.billing.core.admin.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.arasu.billing.core.admin.entity.BranchDCNumberAllocation;
import com.arasu.billing.core.admin.entity.DCNumber;
import com.arasu.billing.core.admin.repository.DCNumberRepository;
import com.arasu.billing.core.common.response.APIResponse;
import com.arasu.billing.core.master.entity.BranchMaster;

@Service
public class DCNumberService {
	@Autowired
	private DCNumberRepository dcNumberRepository;
	@Autowired
	private BranchDCNumberAllocationService branchDCNumberAllocationService;

	public APIResponse getCurrentDCRunningNumber(int branchMasterId) {
		APIResponse apiResponse = new APIResponse();
		BranchMaster branchMaster = new BranchMaster();
		branchMaster.setId(branchMasterId);
		apiResponse.setData(getCurrentRunningNumber(branchMaster));
		return apiResponse;
	}

	private int getCurrentRunningNumber(BranchMaster branchMaster) {
		DCNumber dcNumber = dcNumberRepository.findFirstByBranchMasterOrderByIdDesc(branchMaster);
		BranchDCNumberAllocation branchDCNumberAllocation = branchDCNumberAllocationService.getBranchNumber(branchMaster);
		if (null == dcNumber || dcNumber.getDcNumber() == 0 || dcNumber.getDcNumber() == branchDCNumberAllocation.getToNumber()
				|| dcNumber.getDcNumber() > branchDCNumberAllocation.getToNumber() || dcNumber.getDcNumber() < branchDCNumberAllocation.getFromNumber()) {
			return branchDCNumberAllocation.getFromNumber();
		}
		return dcNumber.getDcNumber() + 1;
	}

	@Transactional(rollbackFor = RuntimeException.class, isolation = Isolation.SERIALIZABLE, propagation = Propagation.REQUIRES_NEW)
	public synchronized DCNumber getDCRunningNumber(BranchMaster branchMaster) {
		int currentRunningNumber = getCurrentRunningNumber(branchMaster);
		DCNumber dcNumber = new DCNumber();
		dcNumber.setBranchMaster(branchMaster);
		dcNumber.setDcNumber(currentRunningNumber);
		dcNumber.setStatus(Boolean.TRUE);
		return dcNumberRepository.save(dcNumber);
	}

}
