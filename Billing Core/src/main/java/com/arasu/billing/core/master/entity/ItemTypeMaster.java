package com.arasu.billing.core.master.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.arasu.billing.core.common.to.BaseEntity;

@Entity
@Table(name = "item_type_master")
public class ItemTypeMaster extends BaseEntity implements Serializable{
	private static final long serialVersionUID = 1L;

	@Column(name = "item_type_name", unique = true)
	private String itemTypeName;

	public String getItemTypeName() {
		return itemTypeName;
	}

	public void setItemTypeName(String itemTypeName) {
		this.itemTypeName = itemTypeName;
	}

}
