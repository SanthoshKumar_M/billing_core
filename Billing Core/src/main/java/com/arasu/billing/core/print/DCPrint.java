package com.arasu.billing.core.print;

import javax.print.Doc;
import javax.print.DocFlavor;
import javax.print.DocPrintJob;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.SimpleDoc;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.HashPrintServiceAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.PrintServiceAttributeSet;
import javax.print.attribute.standard.PrinterName;

public class DCPrint {

	public DCPrint() {
		// TODO Auto-generated constructor stub
	}

	public String rawprint(String fileName) {
//		InputStream inputStream = null;
//		try {
//			inputStream = new FileInputStream("D:/" + fileName + ".pdf");
//		} catch (FileNotFoundException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}
		String res = "";
		PrintServiceAttributeSet printServiceAttributeSet = new HashPrintServiceAttributeSet();
		String printerName = "TVS MSP 250 Star";
		printServiceAttributeSet.add(new PrinterName(printerName, null));
		PrintService printServices[] = PrintServiceLookup.lookupPrintServices(null, printServiceAttributeSet);
		if (printServices.length != 1) {
			return "Can't  select printer :" + printerName;
		}
		PrintService pservice = printServices[0];

//		PDDocument document=null;
//		try {
//			document = PDDocument.load(new File("D:/" + fileName + ".pdf"));
////			document.getPage(0).setRotation(-90);
//			PrinterJob job = PrinterJob.getPrinterJob();
//			job.setPageable(new PDFPageable(document));
//			job.setPrintService(pservice);
//			job.print();
//			document.close();
//		} catch (IOException | PrinterException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//
////		Doc pdfDoc = new SimpleDoc(inputStream, null, null);
////		DocPrintJob printJob = pservice.createPrintJob();
////		try {
////			printJob.print(pdfDoc, new HashPrintRequestAttributeSet());
////		} catch (PrintException e) {
////			// TODO Auto-generated catch block
////			e.printStackTrace();
////		}

//        InputStream result = IOUtils.toInputStream(fileName, StandardCharsets.UTF_16BE);
		byte[] printdata = fileName.getBytes();
       
		DocPrintJob job = pservice.createPrintJob();
		DocFlavor flavor = DocFlavor.BYTE_ARRAY.AUTOSENSE;
		Doc doc = new SimpleDoc(printdata, flavor, null);
		PrintRequestAttributeSet aset = new HashPrintRequestAttributeSet();
		try {
			job.print(doc, aset);
		} catch (Exception e) {
			res = e.getMessage();

		}
		return res;
	}

}
