package com.arasu.billing.core.billing.repository;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.arasu.billing.core.billing.entity.DC;
import com.arasu.billing.core.master.entity.EmployeeMaster;

public interface DCRepository extends JpaRepository<DC, Integer> {
	public static final String QUERY = "SELECT dcline.dc.id as id ,"
			+ "dcline.dc.dCNo as dCNo, "
			+ "dcline.dc.dCDate as dCDate," 
			+ "dcline.dc.fromCustomerMaster.id as fromCustomerMasterId,"
			+ "dcline.dc.fromCustomerMaster.customerName as fromCustomerMasterName," 
			+ "dcline.dc.fromCustomerMaster.customerPrintName as fromCustomerPrintName,"
			+ "dcline.dc.fromCustomerMaster.cityMaster.id as fromCityMasterId," 
			+ "dcline.dc.fromCustomerMaster.cityMaster.cityName as fromCityMastercityName,"
			+ "dcline.dc.toCustomerMaster.id as toCustomerMasterId," 
			+ "dcline.dc.toCustomerMaster.customerName as toCustomerMasterName,"
			+ "dcline.dc.toCustomerMaster.customerPrintName as toCustomerPrintName," 
			+ "dcline.dc.toCustomerMaster.mobileNumber as mobileNumber,"
			+ "dcline.dc.toCustomerMaster.cityMaster.id as toCityMasterId,"
			+ "dcline.dc.toCustomerMaster.cityMaster.cityName as toCityMastercityName,"
			+ "dcline.dc.toCustomerMobileNumber as toCustomerMobileNumber,"
			+ "dcline.dc.branchMaster.id as branchMasterId, "
			+ "dcline.dc.remarks as remarks,"
			+ "dcline.dc.amount as amount, "
			+ "dcline.dc.otherChargesDetails as otherChargesDetails,"
			+ "dcline.dc.otherChargesAmount as otherChargesAmount, "
			+ "dcline.dc.modeOfPay as modeOfPay,"
			+ "dcline.dc.deliveryStatus as deliveryStatus,"
			+ "dcline.dc.receivedAmount as receivedAmount, "
			+ "dcline.dc.officeId as officeId, "
			+ "dcline.dc.officeDelivery as officeDelivery, "
			+ "dcline.dc.officeDelivered as officeDelivered, "
			+ "dcline.dc.rowStatus as rowStatus, "
			+ "dcline.dc.createUser.id as createUserId,"
			+ "dcline.dc.createDateTime as createDateTime,"
			+ "dcline.id as dcLineId, "
			+ "dcline.itemMaster.id as itemMasterId,"
			+ "dcline.itemMaster.itemName as itemMasterName,"
			+ "dcline.qty as itemQty,"
			+ "dcline.rowStatus as dclineRowStatus,"
			+ "dcline.createUser.id as dcLineCreateUserId,"
			+ "dcline.createDateTime as dcLineCreateDateTime " 
			+ "FROM DCLine dcline "
			+ "left outer join dcline.dc " 
			+ "left outer join dcline.dc.fromCustomerMaster " 
			+ "left outer join dcline.dc.fromCustomerMaster.cityMaster "
			+ "left outer join dcline.dc.toCustomerMaster " 
			+ "left outer join dcline.dc.toCustomerMaster.cityMaster ";
	
	

	@Query(value = "select dc.id as id, dc.dc_no as dcNo,dc.dc_date_and_time as dcDate,"
			+ "fromCustomer.id as fromCustomerMasterId,fromCustomer.customer_name as fromCustomerMasterName,"
			+ "toCustomer.id as toCustomerMasterId,toCustomer.customer_name as toCustomerMasterName,"
			+ "dc.row_status as rowStatus, dc.delivery_status as deliveryStatus,dc.received_amount as receivedAmount,"
			+ "createdEmp.id as createUserId,createdEmp.employee_name as createUserName,dc.create_date_and_time as createDateTime "
			+ " from dc as dc"
			+ " left join customer_master as fromCustomer on fromCustomer.id=dc.from_customer_master_id"
			+ " join  customer_master as toCustomer on toCustomer.id=dc.to_customer_master_id "
			+ " join  branch_master as branch on branch.id=dc.branch_master_id"
			+ " join  employee_master as createdEmp on createdEmp.id=dc.create_user_id"
			+ " where branch.id=:branchMasterId ORDER BY dc.id DESC LIMIT 300", nativeQuery = true)
	List<Map<String, Object>> getDCList(int branchMasterId);

	@Query(QUERY + "where dcline.rowStatus<>:deleted and dcline.dc.id=:id")
	List<Map<String, Object>> getDCDetails(int id, Integer deleted);

	@Query(value = "select dc.id as id, dc.dc_no as dcNo,dc.dc_date_and_time as dcDate,"
			+ "fromCustomer.id as fromCustomerMasterId,fromCustomer.customer_name as fromCustomerMasterName,"
			+ "toCustomer.id as toCustomerMasterId,toCustomer.customer_name as toCustomerMasterName,"
			+ "dc.row_status as rowStatus, dc.delivery_status as deliveryStatus,dc.received_amount as receivedAmount from dc as dc"
			+ " left join customer_master as fromCustomer on fromCustomer.id=dc.from_customer_master_id"
			+ " join  customer_master as toCustomer on toCustomer.id=dc.to_customer_master_id join  branch_master as branch on branch.id=dc.branch_master_id"
			+ " where branch.id=:branchMasterId and dc.dc_no=:dCNo ORDER BY dc.id DESC LIMIT 1000", nativeQuery = true)
	List<Map<String, Object>> findAllByBranchMasterAndDCNo(int branchMasterId, int dCNo);

	@Modifying
	@Query("Update DC dc set dc.amount=:amount,dc.netAmount=:netAmount, dc.driverName.id=:driverId,dc.deliveredBy.id=:driverId,dc.amountReceivedBy=:amountRecivedBy,dc.vehicleMaster.id=:vehicleId,dc.officeDelivery=:isOfficeDeliver,dc.officeId=:branchId where  dc.id=:id ")
	int updateDriverAndVehicleDetails(int id, BigDecimal amount, BigDecimal netAmount, int driverId, int vehicleId, EmployeeMaster amountRecivedBy, boolean isOfficeDeliver, int branchId);

	@Modifying
	@Query("Update DC dc set dc.deliveredBy.id=:deliveredById,dc.amountReceivedBy=:amountRecivedBy where  dc.id=:id ")
	int updateDeliveryandPaymentDriverDetails(int id, int deliveredById, EmployeeMaster amountRecivedBy);

	@Modifying
	@Query("Update DC dc set dc.deliveryStatus=:deliveryStatus,dc.deliveredDate=:deliveredDate,dc.receivedAmount=:receivedAmount ,dc.amountReceivedDate=:amountReceivedDate ,dc.rowStatus=:dcStatus,"
			+ "dc.dCAmountCheckBy=:dCAmountCheckBy,dc.dCAmountCheckDate=:dCAmountCheckDate where  dc.id=:id ")
	int updateDCCollectionDetails(int id, int deliveryStatus, Date deliveredDate, BigDecimal receivedAmount, Date amountReceivedDate, int dcStatus, EmployeeMaster dCAmountCheckBy, Date dCAmountCheckDate);
	
	@Modifying
	@Query("Update DC dc set dc.deliveryStatus=:deliveryStatusInOffice, dc.officeDelivered=:isReceived,dc.officeDeliveredDate=:receivedDate where  dc.id=:id ")
	int receivedOfficeItemDetails(int id,int deliveryStatusInOffice, Boolean isReceived, Date receivedDate);

//	@Query(QUERY + "where dcline.rowStatus<>:deleted and dcline.dc.id=:id")
//	List<Map<String, Object>> getMasterReport(Integer deleted, java.sql.Date fromDate, java.sql.Date toDate, int customerMasterId,
//			int cityMasterId, int driverId);

}
