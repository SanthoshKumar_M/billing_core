package com.arasu.billing.core.master.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "employee_master")
public class EmployeeMaster implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(name = "employee_name")
	private String employeeName;

	@Column(name = "address")
	private String employeeAddress;

	@Column(name = "mobile_number")
	private String mobileNumber;

	@Column(name = "date_of_joining")
	private Date dateOfJoin;

	@Column(name = "aadhaar_number")
	private String aadhaarNumber;

	@ManyToOne
	@JoinColumn(name = "type_of_employee")
	private EmployeeType employeeType;

	@Column(name = "license_number")
	private String licenseNumber;

	@Column(name = "aadhaar_photo")
	private byte[] aadhaarPhoto;

	@Column(name = "employee_photo")
	private byte[] employeePhoto;

	@Column(name = "license_renewal_date")
	private Date licenseRenewalDate;

	@Column(name = "row_status")
	private Integer rowStatus;

	@Column(name = "create_user_id")
	private Integer createUser;

	@Column(name = "create_date_and_time")
	private Date createDateTime;

	@Column(name = "update_user_id")
	private Integer updateUser;

	@Column(name = "update_date_and_time")
	private Date updateDateTime;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public String getEmployeeAddress() {
		return employeeAddress;
	}

	public void setEmployeeAddress(String employeeAddress) {
		this.employeeAddress = employeeAddress;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public Date getDateOfJoin() {
		return dateOfJoin;
	}

	public void setDateOfJoin(Date dateOfJoin) {
		this.dateOfJoin = dateOfJoin;
	}

	public String getAadhaarNumber() {
		return aadhaarNumber;
	}

	public void setAadhaarNumber(String aadhaarNumber) {
		this.aadhaarNumber = aadhaarNumber;
	}

	public EmployeeType getEmployeeType() {
		return employeeType;
	}

	public void setEmployeeType(EmployeeType employeeType) {
		this.employeeType = employeeType;
	}

	public String getLicenseNumber() {
		return licenseNumber;
	}

	public void setLicenseNumber(String licenseNumber) {
		this.licenseNumber = licenseNumber;
	}

	public byte[] getAadhaarPhoto() {
		return aadhaarPhoto;
	}

	public void setAadhaarPhoto(byte[] aadhaarPhoto) {
		this.aadhaarPhoto = aadhaarPhoto;
	}

	public byte[] getEmployeePhoto() {
		return employeePhoto;
	}

	public void setEmployeePhoto(byte[] employeePhoto) {
		this.employeePhoto = employeePhoto;
	}

	public Date getLicenseRenewalDate() {
		return licenseRenewalDate;
	}

	public void setLicenseRenewalDate(Date licenseRenewalDate) {
		this.licenseRenewalDate = licenseRenewalDate;
	}

	public Integer getRowStatus() {
		return rowStatus;
	}

	public void setRowStatus(Integer rowStatus) {
		this.rowStatus = rowStatus;
	}

	public Integer getCreateUser() {
		return createUser;
	}

	public void setCreateUser(Integer createUser) {
		this.createUser = createUser;
	}

	public Date getCreateDateTime() {
		return createDateTime;
	}

	public void setCreateDateTime(Date createDateTime) {
		this.createDateTime = createDateTime;
	}

	public Integer getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(Integer updateUser) {
		this.updateUser = updateUser;
	}

	public Date getUpdateDateTime() {
		return updateDateTime;
	}

	public void setUpdateDateTime(Date updateDateTime) {
		this.updateDateTime = updateDateTime;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
