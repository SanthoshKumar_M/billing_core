package com.arasu.billing.core.common.data;

import com.arasu.billing.core.admin.entity.Login;
import com.arasu.billing.core.master.entity.EmployeeMaster;

public class EmployeeData {
	private EmployeeMaster employeeMaster;
	private Login login;

	public EmployeeMaster getEmployeeMaster() {
		return employeeMaster;
	}

	public void setEmployeeMaster(EmployeeMaster employeeMaster) {
		this.employeeMaster = employeeMaster;
	}

	public Login getLogin() {
		return login;
	}

	public void setLogin(Login login) {
		this.login = login;
	}

}
