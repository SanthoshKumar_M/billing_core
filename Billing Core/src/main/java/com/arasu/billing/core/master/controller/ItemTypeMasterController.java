package com.arasu.billing.core.master.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.arasu.billing.core.common.response.APIResponse;
import com.arasu.billing.core.master.service.ItemTypeMasterService;

@RestController
public class ItemTypeMasterController {
	@Autowired
	private ItemTypeMasterService itemTypeMasterService;

	@GetMapping("/getAllItemTypeDetails")
	public APIResponse findAllItem() {
		return itemTypeMasterService.getAllItemTypeDetails();
	}

}
