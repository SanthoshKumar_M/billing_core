package com.arasu.billing.core.master.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.arasu.billing.core.common.to.BaseEntity;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@Entity
@Table(name = "branch_master")
public class BranchMaster extends BaseEntity {

	@Column(name = "branch_name")
	private String branchName;

	@Column(name = "address")
	private String branchAddress;

	@Column(name = "pincode")
	private int pincode;

	@JsonInclude(value = Include.NON_NULL)
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "city_master_id")
	private CityMaster cityMaster;
	
	@Column(name = "printer_name")
	private String printerName;

	@JsonInclude(value = Include.NON_NULL)
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "company_master_id")
	private CompanyMaster companyMaster;

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getBranchAddress() {
		return branchAddress;
	}

	public void setBranchAddress(String branchAddress) {
		this.branchAddress = branchAddress;
	}

	public int getPincode() {
		return pincode;
	}

	public void setPincode(int pincode) {
		this.pincode = pincode;
	}

	public CityMaster getCityMaster() {
		return cityMaster;
	}

	public void setCityMaster(CityMaster cityMaster) {
		this.cityMaster = cityMaster;
	}

	public String getPrinterName() {
		return printerName;
	}

	public void setPrinterName(String printerName) {
		this.printerName = printerName;
	}

	public CompanyMaster getCompanyMaster() {
		return companyMaster;
	}

	public void setCompanyMaster(CompanyMaster companyMaster) {
		this.companyMaster = companyMaster;
	}

}
