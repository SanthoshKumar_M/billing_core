package com.arasu.billing.core.billing.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.arasu.billing.core.common.to.BaseEntity;
import com.arasu.billing.core.master.entity.BranchMaster;
import com.arasu.billing.core.master.entity.CustomerMaster;
import com.arasu.billing.core.master.entity.EmployeeMaster;
import com.arasu.billing.core.master.entity.VehicleMaster;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@Entity
@Table(name = "dc")
public class DC extends BaseEntity{
	
	@Column(name = "dc_no")
	private int dCNo;

	@Column(name = "dc_date_and_time")
	private Date dCDate;

	@JsonInclude(value = Include.NON_NULL)
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "from_customer_master_id")
	private CustomerMaster fromCustomerMaster;
	
	@Column(name = "to_customer_mobile_number")
	private String toCustomerMobileNumber;
	
	@JsonInclude(value = Include.NON_NULL)
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "to_customer_master_id")
	private CustomerMaster toCustomerMaster;

	@Column(name = "remarks")
	private String remarks;

	@Column(name = "total_no_of_qty")
	private BigDecimal totalNoOfQty;

	@Column(name = "amount")
	private BigDecimal amount;

	@Column(name = "other_charges_details")
	private String otherChargesDetails;

	@Column(name = "other_charges_amount")
	private BigDecimal otherChargesAmount;

	@Column(name = "net_amount")
	private BigDecimal netAmount;

	@Column(name = "mode_of_pay")
	private int modeOfPay;

	@JsonInclude(value = Include.NON_NULL)
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "vehicle_master_id")
	private VehicleMaster vehicleMaster;

	@JsonInclude(value = Include.NON_NULL)
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "driver_id")
	private EmployeeMaster driverName;

	@Column(name = "delivery_status")
	private int deliveryStatus;
	
	@JsonInclude(value = Include.NON_NULL)
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "delivered_by")
	private EmployeeMaster deliveredBy;

	@Column(name = "delivered_date")
	private Date deliveredDate;

	@Column(name = "received_amount")
	private BigDecimal receivedAmount;
	
	@JsonInclude(value = Include.NON_NULL)
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "amount_received_by")
	private EmployeeMaster amountReceivedBy;
	
	@Column(name = "amount_received_date")
	private Date amountReceivedDate;
	
	@Column(name = "office_id")
	private int officeId;
	
	@Column(name = "is_office_delivery")
	private boolean officeDelivery;
	
	@Column(name = "is_office_delivered")
	private boolean officeDelivered;
	
	@Column(name = "office_delivered_date")
	private Date officeDeliveredDate;
	
	@JsonInclude(value = Include.NON_NULL)
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "dc_and_amount_check_by")
	private EmployeeMaster dCAmountCheckBy;
	
	@Column(name = "dc_and_amount_check_date")
	private Date dCAmountCheckDate;
	
	@JsonInclude(value = Include.NON_NULL)
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "branch_master_id")
	private BranchMaster branchMaster;

	
	public int getdCNo() {
		return dCNo;
	}

	public void setdCNo(int dCNo) {
		this.dCNo = dCNo;
	}

	public Date getdCDate() {
		return dCDate;
	}

	public void setdCDate(Date dCDate) {
		this.dCDate = dCDate;
	}

	public CustomerMaster getFromCustomerMaster() {
		return fromCustomerMaster;
	}

	public void setFromCustomerMaster(CustomerMaster fromCustomerMaster) {
		this.fromCustomerMaster = fromCustomerMaster;
	}

	public CustomerMaster getToCustomerMaster() {
		return toCustomerMaster;
	}

	public void setToCustomerMaster(CustomerMaster toCustomerMaster) {
		this.toCustomerMaster = toCustomerMaster;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public BigDecimal getTotalNoOfQty() {
		return totalNoOfQty;
	}

	public void setTotalNoOfQty(BigDecimal totalNoOfQty) {
		this.totalNoOfQty = totalNoOfQty;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getOtherChargesDetails() {
		return otherChargesDetails;
	}

	public void setOtherChargesDetails(String otherChargesDetails) {
		this.otherChargesDetails = otherChargesDetails;
	}

	public BigDecimal getOtherChargesAmount() {
		return otherChargesAmount;
	}

	public void setOtherChargesAmount(BigDecimal otherChargesAmount) {
		this.otherChargesAmount = otherChargesAmount;
	}

	public BigDecimal getNetAmount() {
		return netAmount;
	}

	public void setNetAmount(BigDecimal netAmount) {
		this.netAmount = netAmount;
	}

	public int getModeOfPay() {
		return modeOfPay;
	}

	public void setModeOfPay(int modeOfPay) {
		this.modeOfPay = modeOfPay;
	}

	public VehicleMaster getVehicleMaster() {
		return vehicleMaster;
	}

	public void setVehicleMaster(VehicleMaster vehicleMaster) {
		this.vehicleMaster = vehicleMaster;
	}

	public EmployeeMaster getDriverName() {
		return driverName;
	}

	public void setDriverName(EmployeeMaster driverName) {
		this.driverName = driverName;
	}

	public int getDeliveryStatus() {
		return deliveryStatus;
	}

	public void setDeliveryStatus(int deliveryStatus) {
		this.deliveryStatus = deliveryStatus;
	}

	public EmployeeMaster getDeliveredBy() {
		return deliveredBy;
	}

	public void setDeliveredBy(EmployeeMaster deliveredBy) {
		this.deliveredBy = deliveredBy;
	}

	public Date getDeliveredDate() {
		return deliveredDate;
	}

	public void setDeliveredDate(Date deliveredDate) {
		this.deliveredDate = deliveredDate;
	}

	public BigDecimal getReceivedAmount() {
		return receivedAmount;
	}

	public void setReceivedAmount(BigDecimal receivedAmount) {
		this.receivedAmount = receivedAmount;
	}

	public EmployeeMaster getAmountReceivedBy() {
		return amountReceivedBy;
	}

	public void setAmountReceivedBy(EmployeeMaster amountReceivedBy) {
		this.amountReceivedBy = amountReceivedBy;
	}

	public Date getAmountReceivedDate() {
		return amountReceivedDate;
	}

	public void setAmountReceivedDate(Date amountReceivedDate) {
		this.amountReceivedDate = amountReceivedDate;
	}

	public BranchMaster getBranchMaster() {
		return branchMaster;
	}

	public void setBranchMaster(BranchMaster branchMaster) {
		this.branchMaster = branchMaster;
	}

	public EmployeeMaster getdCAmountCheckBy() {
		return dCAmountCheckBy;
	}

	public void setdCAmountCheckBy(EmployeeMaster dCAmountCheckBy) {
		this.dCAmountCheckBy = dCAmountCheckBy;
	}

	public Date getdCAmountCheckDate() {
		return dCAmountCheckDate;
	}

	public void setdCAmountCheckDate(Date dCAmountCheckDate) {
		this.dCAmountCheckDate = dCAmountCheckDate;
	}

	public boolean isOfficeDelivery() {
		return officeDelivery;
	}

	public void setOfficeDelivery(boolean officeDelivery) {
		this.officeDelivery = officeDelivery;
	}

	public boolean isOfficeDelivered() {
		return officeDelivered;
	}

	public void setOfficeDelivered(boolean officeDelivered) {
		this.officeDelivered = officeDelivered;
	}

	public Date getOfficeDeliveredDate() {
		return officeDeliveredDate;
	}

	public void setOfficeDeliveredDate(Date officeDeliveredDate) {
		this.officeDeliveredDate = officeDeliveredDate;
	}

	public int getOfficeId() {
		return officeId;
	}

	public void setOfficeId(int officeId) {
		this.officeId = officeId;
	}

	public String getToCustomerMobileNumber() {
		return toCustomerMobileNumber;
	}

	public void setToCustomerMobileNumber(String toCustomerMobileNumber) {
		this.toCustomerMobileNumber = toCustomerMobileNumber;
	}
}
