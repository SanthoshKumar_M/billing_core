package com.arasu.billing.core.admin.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.arasu.billing.core.admin.repository.LoginRepository;
import com.arasu.billing.core.common.response.APIResponse;

@Service
public class LoginService {
	@Autowired
	private LoginRepository loginRepository;

	public APIResponse getLoginDetails(String userName, String password) {
		APIResponse apiResponse = new APIResponse();
		apiResponse.setData(loginRepository.findLoginUser(userName, password));
		return apiResponse;
	}

}
