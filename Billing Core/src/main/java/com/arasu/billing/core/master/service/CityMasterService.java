package com.arasu.billing.core.master.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.arasu.billing.core.common.response.APIResponse;
import com.arasu.billing.core.master.entity.CityMaster;
import com.arasu.billing.core.master.repository.CityMasterRepository;

@Service
public class CityMasterService {

	@Autowired
	private CityMasterRepository cityMasterRepository;

	public APIResponse getAllCityDetails() {
		APIResponse apiResponse = new APIResponse();
		List<Map<String, Object>> searchResult = cityMasterRepository.getAllCity();
		apiResponse.setData(convertCityMaptoList(searchResult));
		apiResponse.setError(null != apiResponse.getData() ? null : "City Null");
		return apiResponse;
	}

	private List<CityMaster> convertCityMaptoList(final List<Map<String, Object>> searchResult) {
		List<CityMaster> cityMasters = new ArrayList<>();
		if (null != searchResult) {
			for (int i = 0; i < searchResult.size(); i++) {
				CityMaster cityMaster = new CityMaster();
				Map<String, Object> map = searchResult.get(i);
				cityMaster.setId(Integer.parseInt(map.get("id").toString()));
				cityMaster.setCityName(map.get("cityName").toString());
				cityMasters.add(cityMaster);
			}
		}
		return cityMasters;

	}

}
