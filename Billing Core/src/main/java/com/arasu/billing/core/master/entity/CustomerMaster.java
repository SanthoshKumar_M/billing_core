package com.arasu.billing.core.master.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.arasu.billing.core.common.to.BaseEntity;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@Entity
@Table(name = "customer_master")
public class CustomerMaster extends BaseEntity {

	@Column(name = "customer_name", unique = true)
	private String customerName;

	@Column(name = "customer_print_name")
	private String customerPrintName;

	@Column(name = "customer_address")
	private String customerAddress;

	@JsonInclude(value = Include.NON_NULL)
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "city_master_id")
	private CityMaster cityMaster;

	@Column(name = "mobile_number")
	private String mobileNumber;

	@Column(name = "alternative_mobile_number")
	private String alternativeMobileNumber;

	@Column(name = "phone_number")
	private String phoneNumber;

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerPrintName() {
		return customerPrintName;
	}

	public void setCustomerPrintName(String customerPrintName) {
		this.customerPrintName = customerPrintName;
	}

	public String getCustomerAddress() {
		return customerAddress;
	}

	public void setCustomerAddress(String customerAddress) {
		this.customerAddress = customerAddress;
	}

	public CityMaster getCityMaster() {
		return cityMaster;
	}

	public void setCityMaster(CityMaster cityMaster) {
		this.cityMaster = cityMaster;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getAlternativeMobileNumber() {
		return alternativeMobileNumber;
	}

	public void setAlternativeMobileNumber(String alternativeMobileNumber) {
		this.alternativeMobileNumber = alternativeMobileNumber;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

}
