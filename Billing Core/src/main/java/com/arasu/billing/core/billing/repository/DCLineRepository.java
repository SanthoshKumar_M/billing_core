package com.arasu.billing.core.billing.repository;

import java.sql.Date;
import java.util.List;
import java.util.Map;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.arasu.billing.core.billing.entity.DCLine;

public interface DCLineRepository extends JpaRepository<DCLine, Integer> {

	public static final String QUERY = "SELECT dc.id AS id, "
			+ "dc.dc_no AS dCNo,"
			+ "dc.dc_date_and_time AS dCDate,"
			+ "fromCustomer.customer_name AS fromCustomerName,"
			+ "fromCity.city_name AS fromCityName,"
			+ "toCity.city_name AS toCityName,"
			+ "toCustomer.customer_name AS toCustomerName,"
			+ "GROUP_CONCAT(CONCAT(itemMaster.item_print_name),'-',dcLine.qty,'\n') AS itemNames,"
			+ "SUM(dcLine.qty) AS totalNoOfQty,dc.amount AS amount,"
			+ "dc.other_charges_details AS otherChargesDetails,"
			+ "dc.other_charges_amount AS otherChargesAmount,"
			+ "dc.net_amount AS netAmount, "
			+ "dc.delivery_status AS deliveryStatus, "
			+ "dc.received_amount as receivedAmount,"
			+ "dc.amount_received_date as amountReceivedDate,"
			+ "dc.delivered_date as deliveredDate,"
			+ "CAST(IFNULL(dc.delivered_by,0) AS UNSIGNED) as deliveredBy,"
			+ "CAST(IFNULL(dc.amount_received_by,0) AS UNSIGNED) as amountReceivedBy,"
			+ "dc.mode_of_pay AS modeOfPayment,"
			+ "branch.branch_name as branchName ,"
			+ "dc.office_id AS officeId,"
			+ "dc.is_office_delivery AS officeDelivery,"
			+ "allocatedDriver.employee_name as allocatedDriverName,"
			+ "IFNULL(deliveryEmp.employee_name,'') as deliveryByName,"
			+ "IFNULL(amountReceivedEmp.employee_name,'') as amountReceivedByName,"
			+ "dc.row_status as rowStatus "
			+ "FROM dc_line AS dcLine "
			+ "JOIN dc AS dc ON dc.id=dcLine.dc_id "
			+ "LEFT JOIN customer_master AS fromCustomer ON fromCustomer.id=dc.from_customer_master_id "
			+ "LEFT JOIN city_master AS fromCity ON fromCity.id=fromCustomer.city_master_id "
			+ "JOIN customer_master AS toCustomer ON toCustomer.id=dc.to_customer_master_id "
			+ "JOIN city_master AS toCity ON toCity.id=toCustomer.city_master_id "
			+ "JOIN item_master AS itemMaster ON itemMaster.id=dcLine.item_master_id "
			+ "JOIN  branch_master AS branch ON branch.id=dc.branch_master_id "
			+ "LEFT JOIN employee_master AS allocatedDriver ON allocatedDriver.id=dc.driver_id "
			+ "LEFT JOIN employee_master AS deliveryEmp ON deliveryEmp.id=dc.delivered_by "
			+ "LEFT JOIN employee_master AS amountReceivedEmp ON amountReceivedEmp.id=dc.amount_received_by ";

	@Modifying
	@Query("Update DCLine dcLine set dcLine.rowStatus=:deleted where  dcLine.id IN(:list) ")
	int updateRemovedDCLineRowStatus(List<Integer> list, Integer deleted);

	@Query(value = QUERY + "WHERE dc.row_status=:dcStatusActive AND dcLine.row_status=:dcLineStatusActive AND dc.delivery_status=:deliveryStatusPending AND branch.id IN(:branchMasterIds) AND dc.dc_date_and_time=:selectedDate AND toCity.id IN(:selectedCitIds) "
			+ "GROUP BY dc.id ORDER BY dc.id ", nativeQuery = true)
	List<Map<String, Object>> getDCDetails(int dcStatusActive, Integer dcLineStatusActive, Integer deliveryStatusPending, List<Integer> branchMasterIds, Date selectedDate, List<Integer> selectedCitIds);

	@Query(value = QUERY + "WHERE dcLine.row_status=:dcLineStatusActive AND dc.row_status=:dcStatusActive AND((dc.delivery_status=:deliveryStatusPending "
			+ "AND dc.delivered_by=:selectedDriverId) OR(dc.received_amount=0 AND dc.amount_received_by=:selectedDriverId )) AND dc.delivery_status!=:deliveryStatusOffice "
			+ "GROUP BY dc.id ORDER BY dc.id ", nativeQuery = true)
	List<Map<String, Object>> getDriverDCList(int dcStatusActive, Integer dcLineStatusActive, int deliveryStatusPending,Integer deliveryStatusOffice, int selectedDriverId);

	@Query(value = QUERY + "WHERE dcLine.row_status=:dcStatusActive AND dc.office_id=:branchMasterId AND dc.is_office_delivery=1 AND dc.is_office_delivered=0 AND dc.driver_id>0  GROUP BY dc.id ORDER BY dc.id ", nativeQuery = true)
	List<Map<String, Object>> getOfficeItemDCList(Integer dcStatusActive, int branchMasterId);

	@Query(value = QUERY + "WHERE dcLine.row_status=:dcLineStatusActive AND dc.row_status=:dcStatusActive AND (dc.delivery_status=:deliveryStatusOffice or dc.received_amount=0)  "
			+ " AND dc.is_office_delivery=1  AND dc.is_office_delivered=1 AND dc.office_id=:branchId "
			+ "GROUP BY dc.id ORDER BY dc.id ", nativeQuery = true)
	List<Map<String, Object>> getReceivedOfficeItemDCList(int dcStatusActive, Integer dcLineStatusActive,
			Integer deliveryStatusOffice, int branchId);

	@Query(value = QUERY + "WHERE dc.row_status=:dcStatus AND dcLine.row_status=:dcLineStatus "
			+ "AND (fromCustomer.id =:customerMasterId or toCustomer.id =:customerMasterId) "
			+ "AND dc.mode_of_pay !=:modeOfPay  "
			+ "AND dc.dc_date_and_time between :fromDate and :toDate "
			+ "GROUP BY dc.id ORDER BY dc.id,dc.dc_date_and_time", nativeQuery = true)
	List<Map<String, Object>> getMonthlyReport(int dcStatus, int dcLineStatus, Date fromDate,
			Date toDate, int customerMasterId,int modeOfPay);

}
