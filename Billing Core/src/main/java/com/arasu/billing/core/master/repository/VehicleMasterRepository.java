package com.arasu.billing.core.master.repository;

import java.util.List;
import java.util.Map;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.arasu.billing.core.master.entity.VehicleMaster;

@Repository
public interface VehicleMasterRepository extends JpaRepository<VehicleMaster, Integer> {
	@Query("SELECT vehicle.id as id,vehicle.vehicleNumber as vehicleNumber,vehicle.rowStatus as rowStatus FROM VehicleMaster vehicle WHERE vehicle.vehicleNumber like :vehicleNumber% and vehicle.rowStatus=1")
	List<Map<String, Object>> vehicleNumberSuggestion(String vehicleNumber);

	boolean existsByvehicleNumber(String vehicleNumber);

	@Query("SELECT vehicle.id as id,vehicle.vehicleNumber as vehicleNumber,vehicle.rowStatus as rowStatus FROM VehicleMaster vehicle WHERE vehicle.rowStatus=1")
	List<Map<String, Object>> getAllVehicle();

}
