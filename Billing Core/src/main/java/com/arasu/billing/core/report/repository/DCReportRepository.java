package com.arasu.billing.core.report.repository;

import java.sql.Date;
import java.util.List;
import java.util.Map;

public interface DCReportRepository {

	List<Map<String, Object>> getDCDetails(Date fromDate, Date toDate, int customerMasterId, int cityMasterId, int driverId);
}
