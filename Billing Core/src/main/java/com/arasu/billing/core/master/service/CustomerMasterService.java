package com.arasu.billing.core.master.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.arasu.billing.core.common.response.APIResponse;
import com.arasu.billing.core.master.entity.CityMaster;
import com.arasu.billing.core.master.entity.CustomerMaster;
import com.arasu.billing.core.master.entity.EmployeeMaster;
import com.arasu.billing.core.master.repository.CustomerMasterRepository;

@Service
public class CustomerMasterService {
	@Autowired
	private CustomerMasterRepository customerMasterRepository;

	public APIResponse getAllCustomerDetails() {
		APIResponse apiResponse = new APIResponse();
		apiResponse.setData(convertSuggestionCustomerList(customerMasterRepository.getAllCustomerDetails()));
		apiResponse.setError(null != apiResponse.getData() ? null : "Customer Name Suggestion Null");
		return apiResponse;
	}
	
	public APIResponse getAllNewCustomerDetails(List<Integer> avaiableCustomersIds) {
		APIResponse apiResponse = new APIResponse();
		apiResponse.setData(convertSuggestionCustomerList(customerMasterRepository.getAllNewCustomerDetails(avaiableCustomersIds)));
		apiResponse.setError(null != apiResponse.getData() ? null : "Customer Name Suggestion Null");
		return apiResponse;
	}

	@Transactional(rollbackFor = RuntimeException.class)
	public APIResponse saveCustomerDetails(CustomerMaster customerMaster) {
		APIResponse apiResponse = new APIResponse();
		customerMaster.setCreateDateTime(new Date());
		apiResponse.setData(customerMasterRepository.save(customerMaster));
		apiResponse.setError(null != apiResponse.getData() ? null : "Customer Name Suggestion Null");
		return apiResponse;
	}

	public APIResponse customerNameSuggestion(String customerName) {
		APIResponse apiResponse = new APIResponse();
		List<Map<String, Object>> searchResult = customerMasterRepository.customerNameSuggestion(customerName);
		apiResponse.setData(convertSuggestionCustomerList(searchResult));
		apiResponse.setError(null != apiResponse.getData() ? null : "Customer Name Suggestion Null");
		return apiResponse;
	}

	public APIResponse getCustomerDetailsById(int id) {
		APIResponse apiResponse = new APIResponse();
		List<Map<String, Object>> searchResult = customerMasterRepository.getCustomerDetailsById(id);
		apiResponse.setData(convertSuggestionCustomerList(searchResult).get(0));
		apiResponse.setError(null != apiResponse.getData() ? null : "Customer Null");
		return apiResponse;
	}

	private List<CustomerMaster> convertSuggestionCustomerList(final List<Map<String, Object>> searchResult) {
		List<CustomerMaster> customerMasters = new ArrayList<CustomerMaster>();
		if (null != searchResult) {
			for (int i = 0; i < searchResult.size(); i++) {
				CustomerMaster customerMaster = new CustomerMaster();
				Map<String, Object> map = searchResult.get(i);
				customerMaster.setId(Integer.parseInt(map.get("id").toString()));
				customerMaster.setCustomerName(map.get("customerName").toString());
				customerMaster.setCustomerPrintName(null != map.get("customerPrintName") ? map.get("customerPrintName").toString() : "");
				customerMaster.setCustomerAddress(null != map.get("customerAddress") ? map.get("customerAddress").toString() : "");
				customerMaster.setMobileNumber(null != map.get("mobileNumber") ? map.get("mobileNumber").toString() : "");
				customerMaster.setAlternativeMobileNumber(null != map.get("alternativeMobileNumber") ? map.get("alternativeMobileNumber").toString() : "");
				customerMaster.setPhoneNumber(null != map.get("phoneNumber") ? map.get("phoneNumber").toString() : "");
				customerMaster.setRowStatus(null != map.get("rowStatus") ? Integer.parseInt(map.get("rowStatus").toString()) : 1);

				if (null != map.get("cityMasterId")) {
					CityMaster cityMaster = new CityMaster();
					cityMaster.setId(Integer.parseInt(map.get("cityMasterId").toString()));
					cityMaster.setCityName(map.get("cityName").toString());
					customerMaster.setCityMaster(cityMaster);
				}

				if (null != map.get("createUserId")) {
					EmployeeMaster employeeMaster = new EmployeeMaster();
					employeeMaster.setId(Integer.parseInt(map.get("createUserId").toString()));
					employeeMaster.setEmployeeName(null != map.get("createUserName") ? map.get("createUserName").toString() : "");					
					customerMaster.setCreateUser(employeeMaster);
					customerMaster.setCreateDateTime((Date) map.get("createDateTime"));

				}
				customerMasters.add(customerMaster);
			}
		}

		return customerMasters;

	}

	@Transactional(rollbackFor = RuntimeException.class)
	public APIResponse updateCustomerDetails(CustomerMaster customerMaster) {
		APIResponse apiResponse = new APIResponse();
		customerMaster.setUpdateDateTime(new Date());
		apiResponse.setData(null != customerMasterRepository.save(customerMaster) ? customerMaster : null);
		apiResponse.setError(null != apiResponse.getData() ? null : "Customer Not Update Update");
		return apiResponse;
	}

	public APIResponse checkCustomerIsAvailable(String customerName) {
		APIResponse apiResponse = new APIResponse();
		apiResponse.setData(customerMasterRepository.existsByCustomerName(customerName));
		return apiResponse;
	}

}
